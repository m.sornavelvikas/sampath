<?php
/*
Plugin Name: Google Analytics Inserter (sampatjewelers)
Plugin URI: https://sampatjewelers.com
Description: Adds a Google analytics tracking code.
Author: Siva Prasanna K
Version: 1.0
 */
 

function sampatjewelers_google_analytics() { ?>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-46446219-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-46446219-1');
</script>

<?php }
add_action( 'wp_head', 'sampatjewelers_google_analytics', 10 );
 

?>