# CleverWooLayeredNavigation
WooCommerce AJAX Layered Navigation, WooCommerce Product Filter adds advanced product filtering to your WooCommerce shop. 

## Key Features

- New filters: Product selection may be narrowed down to show PRICE, NEW, ON-SALE, and IN-STOCK items
- Attribute multi-select: Unlike in native functionality, several attributes of a single filter may be selected at the same time
No intermediary page reloads - AJAX LOAD: Navigation process is not interrupted by reloads after each attribute selection
- Filter products by reviews and ratings
- Range slider display option for numeric attributes: Apply Price slider display mode for various numeric attributes like price, weight, memory size, megabyte, etc. to make store usage more convenient for the customers. Besides, sliders take a lot less space in the navigation block, so the navigation will be more compact.
- 'From-to' widget for numeric attributes: To filter products by price or any other numeric attribute you can either display price ranges or let customers specify minimal and maximal price limits with a handy widged. It's also possible to show the 'from-to' widget together with other price filter types.
- Advanced options for navigation by brand: Filter products by brand in the navigation block – provide customers with an intuitive way to find exactly what they need.
- Images and labels instead of text for better visual browsing;
- Custom URL alias for filters options;
- SEO-friendly URLs;
- Vertical and Horizontal Navigation;
