<?php
/**
 */

//var_dump($item_id);die;


wp_enqueue_style('zoo-ln-style');
wp_enqueue_style('cleverfont');
wp_enqueue_script('zoo-ln-frontend');
wp_enqueue_script('zoo-ln-frontend-jquery-ui');

$item = \Zoo\Helper\Data\get_filter_item_with_short_code($tag);

$item_id = $item['item_id'];

$data = \Zoo\Frontend\Hook\prepare_data($item_id);

$raw_data = \Zoo\Helper\Data\get_global_config_with_name($item_id, 'general-setting');
$general_setting = (array)json_decode($raw_data);
$raw_data = \Zoo\Helper\Data\get_global_config_with_name($item_id, 'filter-style');
$filter_style = (array)json_decode($raw_data);
$raw_data = \Zoo\Helper\Data\get_global_config_with_name($item_id, 'advanced-setting');
$advanced_setting = (array)json_decode($raw_data);

$advanced_config =  array(
    'filter_preset' => isset($tag) ? $tag : '',
    'jquery_selector_products' => isset($advanced_setting['jquery_selector_products']) ? $advanced_setting['jquery_selector_products'] : 'ul.products',
    'jquery_selector_products_count' => isset($advanced_setting['jquery_selector_products_count']) ? $advanced_setting['jquery_selector_products_count'] : '.woocommerce-result-count',
    'jquery_selector_paging' => isset($advanced_setting['jquery_selector_paging']) ? $advanced_setting['jquery_selector_paging'] : '.woocommerce-pagination',
    'shop_page_info' => array(
        'url' => get_permalink(wc_get_page_id('shop')),
        'title' => Zoo\Frontend\Hook\zoo_ln_get_document_title()
    )
);
$advanced_config = json_encode($advanced_config);

$form_class = 'zoo-ln-filter-form '.$tag;
if (isset($general_setting['instant_filtering']) && $general_setting['instant_filtering'] == 1) {
    $form_class .= ' instant_filtering ';
}

if (isset($advanced_setting['apply_ajax']) && $advanced_setting['apply_ajax'] == 1) {
    $form_class .= ' apply_ajax ';
}

$filter_view_style = isset($filter_style['filter_view_style'])?$filter_style['filter_view_style']:'vertical';
$view_style_class = 'zoo_ln_' . $filter_view_style;

$always_visible = false;
if ($filter_view_style == 'horizontal') {
    if (isset($filter_style['horizontal_alway_visible']) && $filter_style['horizontal_alway_visible'] == 1) {
        $always_visible = true;
    }
}
if(count($data)){
?>
<div class="zoo-layer-nav" id="<?php echo esc_attr('filter-preset-'.$item_id)?>">
    <form id="zoo_ln_form_<?php echo esc_attr($item_id)?>" class="<?php echo esc_attr($form_class); ?>" action="" method="post" data-ln-config="<?php echo esc_attr($advanced_config)?>">
        <?php
        if ($filter_view_style == 'horizontal') {
            ?>
            <div class="zoo-ln-wrap-heading">
                <?php
                if (isset($data['primary'])) { ?>
                    <div class="zoo-ln-primary-col">
                        <?php
                        foreach ($data['primary'] as $item_data) :
                            $content_data = (array)reset($item_data['item_config_value']['filter_config_value']);
                            ?>
                            <?php if ($item_data['item_type'] == 'price'): ?>
                            <?php require ZOO_LN_TEMPLATES_PATH . 'view/price.php'; ?>
                        <?php elseif ($item_data['item_type'] == 'attribute'): ?>
                            <?php require ZOO_LN_TEMPLATES_PATH . 'view/attribute.php'; ?>
                        <?php elseif ($item_data['item_type'] == 'categories'): ?>
                            <?php require ZOO_LN_TEMPLATES_PATH . 'view/categories.php'; ?>
                        <?php elseif ($item_data['item_type'] == 'on-sale'): ?>
                            <?php require ZOO_LN_TEMPLATES_PATH . 'view/on-sale.php'; ?>
                        <?php elseif ($item_data['item_type'] == 'in-stock'): ?>
                            <?php require ZOO_LN_TEMPLATES_PATH . 'view/in-stock.php'; ?>
                        <?php elseif ($item_data['item_type'] == 'review'): ?>
                            <?php require ZOO_LN_TEMPLATES_PATH . 'view/review.php'; ?>
                        <?php elseif ($item_data['item_type'] == 'rating'): ?>
                            <?php require ZOO_LN_TEMPLATES_PATH . 'view/rating.php'; ?>
                        <?php elseif ($item_data['item_type'] == 'active-filter'): ?>
                            <?php require ZOO_LN_TEMPLATES_PATH . 'view/active-filter.php'; ?>
                        <?php endif; ?>
                            <?php
                        endforeach;
                        ?>
                    </div>
                    <?php
                }
                if (!$always_visible) { ?>
                    <a class="zoo-ln-btn zoo-toggle-filter-visible"
                       href="javascript:;"><?php echo esc_html__('Filter', 'clever-layered-navigation') ?></a>
                <?php } ?>
            </div>
        <?php }
        if ($filter_view_style == 'horizontal') {
        ?>
        <div class="zoo-ln-wrap-col zoo-ln-<?php echo esc_attr($filter_style["horizontal_number_col"]); ?>-cols"
             <?php if (!$always_visible){ ?>style="display: none"<?php } ?>>
            <?php
            }
            foreach ($data as $col_id => $column_data) :
                if ($col_id == 'primary') continue; ?>
                <div class="zoo-wrap-layer-filter">
                    <?php
                    foreach ($column_data as $item_data) :
                        $content_data = (array)reset($item_data['item_config_value']['filter_config_value']);
                        ?>
                        <?php if ($item_data['item_type'] == 'price'): ?>
                        <?php require ZOO_LN_TEMPLATES_PATH . 'view/price.php'; ?>
                    <?php elseif ($item_data['item_type'] == 'attribute'): ?>
                        <?php require ZOO_LN_TEMPLATES_PATH . 'view/attribute.php'; ?>
                    <?php elseif ($item_data['item_type'] == 'categories'): ?>
                        <?php require ZOO_LN_TEMPLATES_PATH . 'view/categories.php'; ?>
                    <?php elseif ($item_data['item_type'] == 'on-sale'): ?>
                        <?php require ZOO_LN_TEMPLATES_PATH . 'view/on-sale.php'; ?>
                    <?php elseif ($item_data['item_type'] == 'in-stock'): ?>
                        <?php require ZOO_LN_TEMPLATES_PATH . 'view/in-stock.php'; ?>
                    <?php elseif ($item_data['item_type'] == 'review'): ?>
                        <?php require ZOO_LN_TEMPLATES_PATH . 'view/review.php'; ?>
                    <?php elseif ($item_data['item_type'] == 'rating'): ?>
                        <?php require ZOO_LN_TEMPLATES_PATH . 'view/rating.php'; ?>
                    <?php elseif ($item_data['item_type'] == 'active-filter'): ?>
                        <?php require ZOO_LN_TEMPLATES_PATH . 'view/active-filter.php'; ?>
                    <?php endif; ?>
                        <?php
                    endforeach;
                    ?>
                </div>
                <?php
            endforeach;
            if ($filter_view_style == 'horizontal') {
            ?>
        </div>
    <?php
    }
    if (isset($advanced_setting['apply_ajax']) && $advanced_setting['apply_ajax'] == 1) {
        $orderby  = isset( $_GET['orderby'] ) ? wc_clean( wp_unslash( $_GET['orderby'] ) ) : apply_filters( 'woocommerce_default_catalog_orderby', get_option( 'woocommerce_default_catalog_orderby' ) ); // WPCS: sanitization ok, input var ok, CSRF ok.

                ?>
        <input type="hidden" name="orderby" value="<?php echo esc_attr($orderby);?>">
        <input type="hidden" name="order" value="<?php echo get_query_var('order');?>">
        <input type="hidden" name="posts_per_page" value="<?php echo get_query_var('posts_per_page');?>">
        <input type="hidden" name="paged" value="">
        <?php
    }
    if (!isset($general_setting['instant_filtering']) || $general_setting['instant_filtering'] != 1) : ?>
        <input type="submit" value="<?php echo esc_attr__('Apply', 'clever-layered-navigation') ?>">
    <?php endif;
//        foreach ($query_arg as $key => $value) {
//            echo('<input type="hidden" name="query_arg[' . $key . ']" id="" value="' . $value . '">');
//        }
        ?>
        <input type="hidden" name="pagination_link" id="" value="<?php echo(esc_url_raw( str_replace( 999999999, '%#%', remove_query_arg( 'add-to-cart', get_pagenum_link( 999999999, false ) ) ) )); ?>">
        <input type="hidden" name="filter_list_id" value="<?php echo($item_id);?>">
        <?php wp_nonce_field('apply_filter', 'zoo_ln_nonce_setting'); ?>
    </form>
</div>
<?php }?>