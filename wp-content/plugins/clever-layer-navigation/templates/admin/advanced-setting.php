<?php
/**
 */

$apply_ajax = isset($data['apply_ajax']) ? strval($data['apply_ajax']) : 0;
$jquery_selector_products = isset($data['jquery_selector_products']) ? strval($data['jquery_selector_products']) : 'ul.products';
$jquery_selector_products_count = isset($data['jquery_selector_products_count']) ? strval($data['jquery_selector_products_count']) : '.woocommerce-result-count';
$jquery_selector_paging = isset($data['jquery_selector_paging']) ? strval($data['jquery_selector_paging']) : '.woocommerce-pagination';
?>

<form action="" method="post">
    <table class="form-table">
        <tbody>
        <tr>
            <th class=""><?php esc_html_e('Enable Ajax Filter','clever-layered-navigation');?></th>
            <td class="">
                <fieldset>
                    <input name="apply_ajax" id="apply_ajax" class="" value="1" type="checkbox" <?php checked($apply_ajax, 1);?>>
                    <label for="apply_ajax">
                        <?php esc_html_e('If check, data will loading by using ajax when filter','clever-layered-navigation');?>
                    </label>
                </fieldset>
            </td>
        </tr>
        <tr>
            <th class=""><?php esc_html_e('Jquery Selector Products','clever-layered-navigation');?></th>
            <td class="">
                <fieldset>
                    <input name="jquery_selector_products" id="jquery_selector_products" class="" value="<?php echo($jquery_selector_products);?>" type="text">
                    <label for="jquery_selector_products">
                        <?php esc_html_e('Use for append content after filter.','clever-layered-navigation');?>
                    </label>
                </fieldset>
            </td>
        </tr>
        <tr>
            <th class=""><?php esc_html_e('Jquery Selector Products Count','clever-layered-navigation');?></th>
            <td class="">
                <fieldset>
                    <input name="jquery_selector_products_count" id="jquery_selector_products_count" class="" value="<?php echo($jquery_selector_products_count);?>" type="text">
                    <label for="jquery_selector_products_count">
                        <?php esc_html_e('Use for append products counts content after filter.','clever-layered-navigation');?>
                    </label>
                </fieldset>
            </td>
        </tr>
        <tr>
            <th class=""><?php esc_html_e('Jquery Selector Paging','clever-layered-navigation');?></th>
            <td class="">
                <fieldset>
                    <input name="jquery_selector_paging" id="jquery_selector_paging" class="" value="<?php echo($jquery_selector_paging);?>" type="text">
                    <label for="jquery_selector_paging">
                        <?php esc_html_e('Use for append paging content after filter.','clever-layered-navigation');?>
                    </label>
                </fieldset>
            </td>
        </tr>
        </tbody>
    </table>
    <input type="submit" class="zoo-ln-button button" value="<?php esc_attr_e('Save','clever-layered-navigation');?>">
    <?php wp_nonce_field( 'advanced_setting', 'zoo_ln_nonce_setting' ); ?>
</form>
