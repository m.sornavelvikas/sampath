<?php
/**
 */

namespace Zoo\Admin\Ajax;

function zoo_ln_save_filter()
{

    $post_data = $_POST;

    if ($post_data['add_new'] == 'multi') {
        $post_data['item-id'] = $post_data['id_base'] . '-' . $post_data['multi_number'];
    }

    $item_list_id = $post_data['item_list_id'];
    $filter_item_name = (string)$post_data['item-id'];
    $filter_item_type = (string)$post_data['item-type'];
    $filter_config_value = json_encode($post_data[$filter_item_type]);
    $filter_column = (string)$post_data['sidebar'];

    $data = array(
        'filter_column' => $filter_column,
        'filter_item_name' => $filter_item_name,
        'filter_item_type' => $filter_item_type,
        'filter_config_value' => $filter_config_value
    );


    \Zoo\Helper\Data\save_filter_config_with_name($item_list_id, $filter_item_name, $data);

    wp_send_json($post_data);

    wp_die();
}

function zoo_ln_save_filter_order()
{

    if (isset($_POST['zoo_ln_nonce_setting']) && wp_verify_nonce($_POST['zoo_ln_nonce_setting'], 'filter_setting')) {
        // process form data
        $post_data = $_POST;
        $data = json_encode($post_data['sidebars']);
        $item_id = $_POST['item_list_id'];

        \Zoo\Helper\Data\save_global_config_with_name($item_id, 'filter-setting-order', $data);
    }
    $data = array(
        'result' => 'done'
    );

    wp_send_json($data);

    wp_die();
}

function zoo_ln_get_product_list()
{


    global $wp_query;
    $GLOBALS['zoo_ln_data']['is_ajax'] = 1;
    $GLOBALS['zoo_ln_data']['need_reset_paging'] = 0;
    $wc_query = new \WP_Query();
    if (isset($_POST['zoo_ln_form_data'])) {
        parse_str($_POST['zoo_ln_form_data'], $post_args);
    } else {
        $post_args = $_POST;
    }

    $wc_query = \Zoo\Frontend\Hook\process_filter($wc_query);

    $arg['post_type'] = 'product';
    $arg['paged'] = isset($post_args['paged']) ? $post_args['paged'] : '1';
    $arg['order'] = $post_args['order'];
    $arg['posts_per_page'] = $post_args['posts_per_page'];
    $arg['meta_key'] = $wc_query->get('meta_key');
    $arg['meta_query'] = $wc_query->get('meta_query');
    $arg['tax_query'] = $wc_query->get('tax_query');
    $arg['product_cat'] = $wc_query->get('product_cat');
    $arg['wc_query'] = 'product_query';
    $arg['post__in'] = $wc_query->get('post__in');
    switch ($post_args['orderby']) {
        case 'menu_order':
            $args['orderby'] = 'menu_order title';
            break;
        case 'title':
            $args['orderby'] = 'title';
            $args['order'] = ('DESC' === $post_args['order']) ? 'DESC' : 'ASC';
            break;
        case 'relevance':
            $args['orderby'] = 'relevance';
            $args['order'] = 'DESC';
            break;
        case 'rand':
            $args['orderby'] = 'rand';
            break;
        case 'date':
            $args['orderby'] = 'date ID';
            $args['order'] = ('ASC' === $post_args['order']) ? 'ASC' : 'DESC';
            break;
        case 'price':
            $args['orderby'] = 'meta_value_num';
            $args['meta_key'] = '_price';
            $args['order'] = 'asc';
            break;
        case 'price-desc':
            $args['orderby'] = 'meta_value_num';
            $args['meta_key'] = '_price';
            $args['order'] = 'desc';
            break;
        case 'popularity':
            $args['orderby'] = array( 'meta_value_num' => 'DESC', 'title' => 'ASC' );
            $args['meta_key'] = 'total_sales';
            break;
        case 'rating':
            $args['meta_key'] = '_wc_average_rating';
            $args['orderby'] = array(
                'meta_value_num' => 'DESC',
                'ID' => 'ASC',
            );
            break;
    }


    $my_query = new \WP_Query(apply_filters('woocommerce_shortcode_products_query', $arg));

    $item_id = 0;
    if (isset($_POST['zoo_ln_form_data'])) {
        parse_str($_POST['zoo_ln_form_data'], $post_option);
        $item_id = ($post_option['filter_list_id']);
    }


    $wp_query = null;
    $wp_query = $my_query;
    ob_start();
    require ZOO_LN_TEMPLATES_PATH . 'product-list.php';
    $html_ul_products_content = ob_get_contents();
    ob_clean();
    woocommerce_result_count();
    $html_result_count_content = ob_get_contents();
    ob_clean();
    wc_get_template('loop/pagination.php');
    $html_pagination_content = ob_get_contents();
    ob_clean();

    $selected_filter_option = \Zoo\Frontend\Hook\get_activated_filter($post_args);
    require ZOO_LN_TEMPLATES_PATH . 'view/active-filter/items.php';
    $html_active_list_item = ob_get_contents();
    ob_end_clean();

    $return['html_ul_products_content'] = $html_ul_products_content;
    $return['html_result_count_content'] = $html_result_count_content;
    $return['html_pagination_content'] = $html_pagination_content;
    $return['html_active_list_item'] = $html_active_list_item;
    wp_send_json($return);
    wp_reset_postdata();
}