;(function ( $ ) {
	'use strict';

	/**
	 * @TODO Code a function the calculate available combination instead of use WC hooks
	 */
	$.fn.tawcvs_variation_swatches_form = function () {
		return this.each( function() {
			var $form = $( this ),
				clicked = null,
				selected = [];

			$form
				.addClass( 'swatches-support' )
				.on( 'click', '.swatch', function ( e ) {
					e.preventDefault();
					var $el = $( this );
					var $select = $el.closest( '.value' ).find( 'select' );

					if($el.hasClass( "swatch-color" ) ){
						
						var $select_ca = $( '.swatch-image' ).closest( '.value' ).find( 'select' );
								var image_selc = $select_ca.val();
							$( '.swatch-image' ).removeClass( 'selected' );
							$select_ca.val( '' ).change();
						
							
					}
					
					
					var	attribute_name = $select.data( 'attribute_name' ) || $select.attr( 'name' );
					var	value = $el.data( 'value' );
					$select.trigger( 'focusin' );
					
					// Check if this combination is available
					if ( ! $select.find( 'option[value="' + value + '"]' ).length ) {
						if($el.hasClass( "swatch-image" ) ){
							$el.siblings( '.swatch' ).removeClass( 'selected' );
							$select.val( '' ).change();
						}
						return;
					}

					clicked = attribute_name;

					if ( selected.indexOf( attribute_name ) === -1 ) {
						selected.push(attribute_name);
					}

					if (($el.hasClass( 'selected' )) && ($el.hasClass('swatch-image'))) {
						$select.val( '' );
						$el.removeClass( 'selected' );
						delete selected[selected.indexOf(attribute_name)];
					}else if (($el.hasClass( 'selected' )) && ($el.hasClass('swatch-label'))) {
						$select.val( '' );
						$el.removeClass( 'selected' );
						delete selected[selected.indexOf(attribute_name)];
					}else if ((!$el.hasClass( 'selected' )) && ($el.hasClass('swatch-image'))){
						$el.addClass( 'selected' ).siblings( '.selected' ).removeClass( 'selected' );
						$select.val( value );
					}else if ((!$el.hasClass( 'selected' )) && ($el.hasClass('swatch-color'))){
						$el.addClass( 'selected' ).parent('.color-container').siblings('.color-container').children('.selected').removeClass( 'selected' );
						$select.val( value );
					}else if ((!$el.hasClass( 'selected' )) && ($el.hasClass('swatch-label'))){
						$el.addClass( 'selected' ).siblings( '.selected' ).removeClass( 'selected' );
						$select.val( value );
					}

					$select.change();
					
					if($el.hasClass( "swatch-color" ) ){
						$select = $( '.swatch-image' ).closest( '.value' ).find( 'select' );
						var y = 0;
						 $(".swatch-image").each(function(i) {
							var value = $( this ).data( "value" );
							if ( ! $select.find( 'option[value="' + value + '"]' ).length ) {
								$( this ).css("display", "none");
							}else{
								$( this ).css("display", "inline-block");
								 if ( y === 0) {
									var selected_color = $("#pa_metal-color").val();
									var myarr = image_selc.split("-");
									myarr = myarr[0] + "-" + myarr[1];
									var selected_image = myarr+'-'+selected_color;
									if(selected_image){
										var $select_cae = $( '.swatch-image' ).closest( '.value' ).find( 'select' );
										$( '.swatch-image' ).removeClass( 'selected' );
										$select_cae.val( '' ).change();
										$( '.swatch-'+selected_image ).addClass( 'selected' );
										$select_cae.val( selected_image ).change();
									}else{
										var $select_cae = $( '.swatch-image' ).closest( '.value' ).find( 'select' );
										$( '.swatch-image' ).removeClass( 'selected' );
										$select_cae.val( '' ).change();
										$( this ).addClass( 'selected' );
										$select_cae.val( value ).change();
									}
								y++;
								}

							}
						 });
						$select = $( '.swatch-label' ).closest( '.value' ).find( 'select' );	 
						 $(".swatch-label").each(function() {
							var value = $( this ).data( "value" );
							if ( ! $select.find( 'option[value="' + value + '"]' ).length ) {
								$( this ).css("display", "none");
							}else{
								$( this ).css("display", "inline-block");
							}
						 });
					}else if($el.hasClass( "swatch-image" ) ){
							$select = $( '.swatch-label' ).closest( '.value' ).find( 'select' );	 
							$(".swatch-label").each(function() {
							var value = $( this ).data( "value" );
							if ( ! $select.find( 'option[value="' + value + '"]' ).length ) {
								$( this ).css("display", "none");
							}else{
								$( this ).css("display", "inline-block");
							}
						 });
					}
					
					
					
				} )
				.on( 'click', '.reset_variations', function () {
					$( this ).closest( '.variations_form' ).find( '.swatch.selected' ).removeClass( 'selected' );
					selected = [];
				} )
				.on( 'tawcvs_no_matching_variations', function() {
					window.alert( wc_add_to_cart_variation_params.i18n_no_matching_variations_text );
				} )
				$(".swatch.swatch-color.selected").trigger( 'click' );
		} );
		}; 

	$( function () {
		$( '.variations_form' ).tawcvs_variation_swatches_form();
		$( document.body ).trigger( 'tawcvs_initialized' );
	} );
})( jQuery );
