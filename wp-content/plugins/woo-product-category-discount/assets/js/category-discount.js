/*
 * This function will handle ajax based category save.
 * Date: 25-04-2017
 * Author: Vidish Purohit
 */

jQuery(document).ready(function() {
	jQuery('.active').on('click', function() {
	    		
		var row_id = jQuery(this).attr('data-row_id');
	    updateCategories(row_id);
	});

	jQuery('.amount').keypress(function(event) {
		  if ((event.which != 46 || jQuery(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
		    event.preventDefault();
		  }
	});
	jQuery('.amount').blur(function(event) {
	  	var row_id = jQuery(this).attr('data-row_id');
	    updateCategories(row_id);
	});
});

function updateCategories(row_id) {
	
	var flgActive = jQuery('.trwcpd' + row_id).find('.active').is(':checked');

	var fltAmount = jQuery('.trwcpd' + row_id).find('.amount').val();
	var type = jQuery('.trwcpd' + row_id).find('.type').val();
	jQuery('.trwcpd' + row_id).find('.complete').fadeOut(300);
	jQuery('input[type="text"], input[type="checkbox"], select').prop("disabled", true);
	jQuery('.trwcpd' + row_id).find('.loader').fadeIn(300);
	jQuery.post(
	    wpcd_obj.admin_url, 
	    {
	        'action': 'wpcd_calculate_discount',
	        'type':   type,
	        'amount': fltAmount,
	        'active': flgActive,
	        'cat_id': row_id
	    }, 
	    function(response){

	        jQuery('.trwcpd' + row_id).find('.loader').css('display', 'none');
	        jQuery('.trwcpd' + row_id).find('.complete').fadeIn(300);
	        jQuery('input[type="text"], input[type="checkbox"], select').removeAttr("disabled");

	        setTimeout(function() {
	        	jQuery('.trwcpd' + row_id).find('.complete').fadeOut(300);
	        }, 3000);
	    }
	);
}

