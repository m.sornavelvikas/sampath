<?php
if ( ! defined( 'ABSPATH' ) ) { 
    exit; // Exit if accessed directly
}

add_action('admin_menu', 'wpcd_register_submenu_page');

function wpcd_register_submenu_page() {
    add_submenu_page( 'woocommerce', __('Product Category Discount'), __('Product Category Discount'), 'manage_options', 'wpcd-category-discount', 'wpcd_product_category_discount' ); 
}

function wpcd_product_category_discount() {

    global $wpdb;
    
    echo '<h3>' . __('Product Category Discount') . '</h3>';
        
    $strCategory = 'SELECT tt.term_id t_id, tt.term_taxonomy_id tt_id, t.name name, tt.parent parent '
            . ' FROM ' . $wpdb->prefix . 'terms t, '
            . $wpdb->prefix . 'term_taxonomy tt '
            . ' WHERE tt.term_id = t.term_id'
            . ' AND tt.taxonomy = "product_cat"';
    $arrCategories = $wpdb->get_results($strCategory);
    
    $arrProcessedCat = array();    
    foreach($arrCategories AS $key => $arrVal) {
        $strCatName = wpcd_get_term_parents($arrVal->t_id, 'product_cat');
        $arrProcessedCat[$arrVal->t_id]['name'] = substr($strCatName, 0, strlen($strCatName) - 3);
        $arrProcessedCat[$arrVal->t_id]['breadcrumb'] = (!empty($arrProcessedCat[$arrVal->parent]['name'])?$arrProcessedCat[$arrVal->parent]['name'] . ' >> ' : '') . $arrVal->name;
        if($arrVal->parent != 0) {
            $arrProcessedCat[$arrVal->parent]['child'][] = $arrVal->t_id;
            $arrProcessedCat[$arrVal->t_id]['isChild'] = 1;
        } else {
            $arrProcessedCat[$arrVal->t_id]['isChild'] = 0;
        }
    }
    
    // Get category discount
    $strCategory = get_option('wpcd_category_discount');
    $arrCatDiscount = unserialize($strCategory);
    
    ?><form enctype="multipart/form-data" method="POST" style="width:100%;float:left;">
        <div>
            <a href="https://www.wooextend.com/product/woo-product-category-discount-pro/" target="_blank">
                <img src="<?php echo plugin_dir_url( __FILE__ ) . 'assets/images/banner.jpg';?>"/>
            </a>
        </div>
        <div class="postbox wc-metaboxes-wrapper" style="width:90%">
            <table style="padding:10px">
                <tbody>
                    <tr>
                        <th>Category</th>
                        <th>Discount type</th>
                        <th>Amount</th>
                        <th>Apply</th>
                    </tr><?php
                    foreach($arrProcessedCat AS $key => $arrVal) {
                        
                        if(!$arrVal['isChild']) {
                            ?><tr class="trwcpd<?php echo $key;?>"><td style="padding:5px;">
                                <label for="txtCatAmount_<?php echo $key;?>"><?php echo $arrProcessedCat[$key]['name']; ?></label>
                            </td>
                            <td style="padding:5px;">
                                <select name="selCat_<?php echo $key;?>" class="type">
                                    <option value="Fixed Amount"><?php _e('Fixed Amount');?></option>
                                    <option value="% of Price" disabled><?php _e('% of Price');?></option>
                                </select>
                            </td>
                            <td style="padding:5px;">
                                <input type='text' data-row_id="<?php echo $key;?>" class="amount" size='12' name="txtCatAmount_<?php echo $key;?>" id="txtCatAmount_<?php echo $key;?>" placeholder="Enter Amount" value="<?php echo isset($arrCatDiscount[$key]['value'])?$arrCatDiscount[$key]['value']:'';?>"/>
                                <?php echo ' ( ' . get_woocommerce_currency_symbol() . ' )';?>
                            </td>
                            <td style="padding:5px;">
                                <input type='checkbox' data-row_id="<?php echo $key;?>" class="active" name="chkActive_<?php echo $key;?>" id="chkActive_<?php echo $key;?>" placeholder="Enter Amount" value="Y" <?php echo isset($arrCatDiscount[$key]['isActive']) && $arrCatDiscount[$key]['isActive'] == 'true'?' checked="checked"':'';?>/>
                            </td>
                            <td class="loader" style="display:none;">
                                <img src="<?php echo plugin_dir_url( __FILE__ ) . 'assets/images/hourglass.gif';?>" style="height:30px;"/>
                            </td>
                            <td class="complete" style="display:none;">
                                <img src="<?php echo plugin_dir_url( __FILE__ ) . 'assets/images/complete.png';?>" style="height:30px;"/>
                            </td></tr><?php
                        }
                        if(!empty($arrVal['child'])) {
                            foreach($arrVal['child'] AS $keyTemp => $childId) {
                                ?><tr class="trwcpd<?php echo $childId;?>"><td style="padding:5px;">
                                    <label for="txtCatAmount_<?php echo $childId;?>"><?php echo $arrProcessedCat[$childId]['name']; ?></label>
                                </td>
                                <td style="padding:5px;">
                                    <select name="selCat_<?php echo $childId;?>" class="type">
                                        <option value="Fixed Amount"><?php _e('Fixed Amount');?></option>
                                        <option value="% of Price" disabled><?php _e('% of Price');?></option>
                                    </select>
                                </td>
                                <td style="padding:5px;">
                                    <input type='text' data-row_id="<?php echo $childId;?>" class="amount" size='12' name="txtCatAmount_<?php echo $childId;?>" id="txtCatAmount_<?php echo $childId;?>" placeholder="Enter Amount" value="<?php echo isset($arrCatDiscount[$childId]['value'])?$arrCatDiscount[$childId]['value']:'';?>"/>
                                    <?php echo ' ( ' . get_woocommerce_currency_symbol() . ' )';?>
                                </td>
                                <td style="padding:5px;">
                                    <input type='checkbox' class="active" name="chkActive_<?php echo $childId;?>" id="chkActive_<?php echo $childId;?>" value="Y" data-row_id="<?php echo $childId;?>" <?php echo isset($arrCatDiscount[$childId]['isActive']) && $arrCatDiscount[$childId]['isActive'] == 'true'?' checked="checked"':'';?>/>
                                </td>
                                <td class="loader" style="display:none;">
                                    <img src="<?php echo plugin_dir_url( __FILE__ ) . 'assets/images/hourglass.gif';?>" style="height:30px;"/>
                                </td>
                                <td class="complete" style="display:none;">
                                    <img src="<?php echo plugin_dir_url( __FILE__ ) . 'assets/images/complete.png';?>" style="height:30px;"/>
                                </td></tr><?php
                            }
                        } 
                    }
                ?></tbody>
            </table>
        </div>
    </form><?php
}

add_action( 'admin_enqueue_scripts', 'load_custom_wp_admin_style' );
function load_custom_wp_admin_style() {
    wp_register_script( 'discount_admin_js', plugin_dir_url( __FILE__ ) . 'assets/js/category-discount.js', false, '1.0.0' );
    wp_enqueue_script( 'discount_admin_js' );
    // Localize the script
    $translation_array = array(
        'admin_url' => admin_url('admin-ajax.php')
    );
    wp_localize_script( 'discount_admin_js', 'wpcd_obj', $translation_array );
}

function wpcd_woocommerce_version_check( $version = '3.0' ) {
  if ( function_exists( 'is_woocommerce_active' ) && is_woocommerce_active() ) {
    global $woocommerce;
    if( version_compare( $woocommerce->version, $version, ">=" ) ) {
      return true;
    }
  }
  return false;
}

add_action( 'wp_ajax_wpcd_calculate_discount', 'wpcd_save_discount' );
add_action( 'wp_ajax_nopriv_wpcd_calculate_discount', 'wpcd_save_discount' );

function wpcd_save_discount() {

    global $wpdb;

    // Get existing saved discounts
    $strCategoryPrice = get_option('wpcd_category_discount');
    $arrCategoryPrice = unserialize($strCategoryPrice);

    $reqCatId = $_POST['cat_id'];
    if(isset($arrCategoryPrice[$reqCatId]) && !empty($arrCategoryPrice[$reqCatId])) {
        $arrSelectedCat = $arrCategoryPrice[$reqCatId];
    } else {
        $arrSelectedCat = array('type' => 'Fixed Amount', 'value' => 0, 'isActive' => 'false');
    }

    $arrCategoryPrice[$reqCatId] = array('type' => 'Fixed Amount', 'value' => $_POST['amount'], 'isActive' => $_POST['active']);
    
    // check if existing and requested both status are inactive, then no need to update product prices
    if($_POST['active'] == 'false' && ($arrSelectedCat['isActive'] == 'false' || empty($arrSelectedCat['isActive']))) {
        update_option('wpcd_category_discount', serialize($arrCategoryPrice));
        die();
    }

    // check if we need to remove applied discount
    if($_POST['active'] == 'false' && $arrSelectedCat['isActive'] == 'true') {

        // Get all products in category
        $strCategory = 'SELECT tr.object_id product_id, pmWpcdCats.meta_value selected_cats, pmPrice.meta_value regular_price'
                    . ' FROM ' . $wpdb->prefix . 'terms t '
                    . ' LEFT JOIN ' . $wpdb->prefix . 'term_taxonomy tt ON (tt.term_id = t.term_id)'
                    . ' LEFT JOIN ' . $wpdb->prefix . 'term_relationships tr ON (tr.term_taxonomy_id = tt.term_taxonomy_id)'
                    . ' LEFT JOIN ' . $wpdb->prefix . 'postmeta pmPrice ON (tr.object_id = pmPrice.post_id AND pmPrice.meta_key = "_regular_price")'
                    . ' LEFT JOIN ' . $wpdb->prefix . 'postmeta pmWpcdCats ON (tr.object_id = pmWpcdCats.post_id AND pmWpcdCats.meta_key = "_wpcd_cats")'
                    . ' WHERE tt.taxonomy = "product_cat"'
                    . ' AND t.term_id = "' . $reqCatId . '"';
        $arrCategories = $wpdb->get_results($strCategory);

        // Loop for each product
        foreach($arrCategories AS $keyPro => $arrPro) {

            if(is_null($arrPro->product_id)) {
                continue;
            }

            $objProduct = wc_get_product( $arrPro->product_id );
            
            if(wpcd_woocommerce_version_check()) {
                $productType = $objProduct->get_type();
                $productId = $arrPro->product_id;
            } else {
                $productType = $objProduct->product_type;
                $productId = $arrPro->product_id;
            }

            if($productType == 'simple') {
                
                // Get all categories that product belongs to
                $arrCats = array();
                $strCats = $arrPro->selected_cats;
                if(isset($strCats) && !empty($strCats)) {
                    $arrCats = explode(',', $strCats);
                }    
                $key = array_search($reqCatId, $arrCats); 
                unset($arrCats[$key]);           
                update_post_meta($productId, '_wpcd_cats', implode(',', array_unique($arrCats)));

                if(!empty($arrCats)) {

                    // Update prices
                    $fltRegularPrice = (float)$arrPro->regular_price;
                    $discountAmount = get_discount_amount($arrCategoryPrice, $arrCats);
                    $newPrice = $fltRegularPrice - $discountAmount;

                    update_post_meta($productId, '_price', $newPrice); 
                    update_post_meta($productId, '_sale_price', $newPrice);
                } else {
                    // Delete wpcd discount
                    delete_post_meta($productId, '_sale_price');
                    delete_post_meta($productId, '_wpcd_cats');
                    // Restore old discounted price
                    $strUpdateKey = "UPDATE {$wpdb->prefix}postmeta SET meta_key = '_sale_price' WHERE meta_key = '_wpcd_sale_price'
                        AND post_id = {$productId}";
                    $wpdb->query($strUpdateKey);

                    $fltRegularPrice = $arrPro->regular_price;
                    $fltSalePrice = get_post_meta($productId, '_sale_price', true);

                    $effectivePrice = (float)(isset($fltSalePrice) && !empty($fltSalePrice)?$fltSalePrice:$fltRegularPrice);
                    update_post_meta($productId, '_price', $effectivePrice); 
                }              
            } else {

                $strVariations = "SELECT p.ID variation_id
                 FROM {$wpdb->prefix}posts p
                 WHERE p.post_type='product_variation' AND p.post_parent = '{$arrPro->product_id}'";
                $arrVariations = $wpdb->get_results($strVariations, ARRAY_A);

                // Loop for each variation 
                foreach($arrVariations AS $keVar => $arrVar) {

                    // Get all categories that product belongs to
                    $arrCats = array();
                    $strCats = $arrPro->selected_cats;
                    if(isset($strCats) && !empty($strCats)) {
                        $arrCats = explode(',', $strCats);
                    }    
                    $key = array_search($reqCatId, $arrCats); 
                    unset($arrCats[$key]);           
                    update_post_meta($arrVar['variation_id'], '_wpcd_cats', implode(',', array_unique($arrCats)));

                    if(!empty($arrCats)) {

                        // Update prices
                        $fltRegularPrice = (float)get_post_meta($arrVar['variation_id'], '_regular_price', true);
                        $discountAmount = get_discount_amount($arrCategoryPrice, $arrCats);
                        $newPrice = $fltRegularPrice - $discountAmount;

                        update_post_meta($arrVar['variation_id'], '_price', $newPrice); 
                        update_post_meta($arrVar['variation_id'], '_sale_price', $newPrice);
                    } else {

                        // Delete wpcd discount
                        delete_post_meta($arrVar['variation_id'], '_sale_price');
                        delete_post_meta($arrVar['variation_id'], '_wpcd_cats');
                        // Restore old discounted price
                        $strUpdateKey = "UPDATE {$wpdb->prefix}postmeta SET meta_key = '_sale_price' WHERE meta_key = '_wpcd_sale_price'
                            AND post_id = {$arrVar['variation_id']}";
                        $wpdb->query($strUpdateKey);

                        $fltRegularPrice = get_post_meta($arrVar['variation_id'], '_regular_price', true);
                        $fltSalePrice = get_post_meta($arrVar['variation_id'], '_sale_price', true);

                        $effectivePrice = (float)(isset($fltSalePrice) && !empty($fltSalePrice)?$fltSalePrice:$fltRegularPrice);
                        update_post_meta($arrVar['variation_id'], '_price', $effectivePrice);
                    }
                }
            }
        }
        $arrCategoryPrice[$reqCatId] = array('type' => 'Fixed Amount', 'value' => $_POST['amount'], 'isActive' => $_POST['active']);
        update_option('wpcd_category_discount', serialize($arrCategoryPrice));
        die();
    }

    // check if we need to apply discount
    if($_POST['active'] == 'true') {

        // Get all products in category
        $strCategory = 'SELECT tr.object_id product_id, pmWpcdCats.meta_value selected_cats, pmPrice.meta_value regular_price'
                    . ' FROM ' . $wpdb->prefix . 'terms t '
                    . ' LEFT JOIN ' . $wpdb->prefix . 'term_taxonomy tt ON (tt.term_id = t.term_id)'
                    . ' LEFT JOIN ' . $wpdb->prefix . 'term_relationships tr ON (tr.term_taxonomy_id = tt.term_taxonomy_id)'
                    . ' LEFT JOIN ' . $wpdb->prefix . 'postmeta pmPrice ON (tr.object_id = pmPrice.post_id AND pmPrice.meta_key = "_regular_price")'
                    . ' LEFT JOIN ' . $wpdb->prefix . 'postmeta pmWpcdCats ON (tr.object_id = pmWpcdCats.post_id AND pmWpcdCats.meta_key = "_wpcd_cats")'
                    . ' WHERE tt.taxonomy = "product_cat"'
                    . ' AND t.term_id = "' . $reqCatId . '"';
        $arrCategories = $wpdb->get_results($strCategory);
        // Loop for each product
        foreach($arrCategories AS $keyPro => $arrPro) {

            if(is_null($arrPro->product_id) || is_null(get_post($arrPro->product_id))) {
                continue;
            }
            $objProduct = wc_get_product( $arrPro->product_id );
            
            if(wpcd_woocommerce_version_check()) {
                $productType = $objProduct->get_type();
                $productId = $arrPro->product_id;
            } else {
                $productType = $objProduct->product_type;
                $productId = $arrPro->product_id;
            }

            if($productType == 'simple') {
                
                // Backup existing discount price if not saved
                $strGetKey = "SELECT * FROM {$wpdb->prefix}postmeta WHERE meta_key = '_wpcd_sale_price'
                    AND post_id = {$productId}";
                $arrKey = $wpdb->get_results($strGetKey);

                if(empty($arrKey)) {
                    $strUpdateKey = "UPDATE {$wpdb->prefix}postmeta SET meta_key = '_wpcd_sale_price' WHERE meta_key = '_sale_price'
                        AND post_id = {$productId}";
                    if(!$wpdb->query($strUpdateKey)) {
                        update_post_meta($productId, '_wpcd_sale_price', $newPrice);
                    }
                }
                
                // Get all categories that product belongs to
                $arrCats = array();
                $strCats = $arrPro->selected_cats;
                if(isset($strCats) && !empty($strCats)) {
                    $arrCats = explode(',', $strCats);
                }
                $arrCats[] = $reqCatId;                
                update_post_meta($productId, '_wpcd_cats', implode(',', array_unique($arrCats)));

                // Update prices
                $fltRegularPrice = (float)$arrPro->regular_price;
                $discountAmount = get_discount_amount($arrCategoryPrice, $arrCats);
                $newPrice = $fltRegularPrice - $discountAmount;

                update_post_meta($productId, '_price', $newPrice); 
                update_post_meta($productId, '_sale_price', $newPrice);
            } else {

                $strVariations = "SELECT p.ID variation_id, pmPrice.meta_value regular_price, wpcdSalePrice.meta_value wpcd_sale_price
                 FROM {$wpdb->prefix}posts p
                 LEFT JOIN {$wpdb->prefix}postmeta pmPrice ON (p.ID = pmPrice.post_id AND pmPrice.meta_key = '_regular_price')
                 LEFT JOIN {$wpdb->prefix}postmeta wpcdSalePrice ON (p.ID = wpcdSalePrice.post_id AND wpcdSalePrice.meta_key = '_wpcd_sale_price')
                 WHERE p.post_type='product_variation' AND p.post_parent = '{$arrPro->product_id}'";
                $arrVariations = $wpdb->get_results($strVariations, ARRAY_A);

                // Loop for each variation 
                foreach($arrVariations AS $keVar => $arrVar) {

                    // Backup existing discount price if not saved
                    if(empty($arrVar['wpcd_sale_price'])) {
                        $strUpdateKey = "UPDATE {$wpdb->prefix}postmeta SET meta_key = '_wpcd_sale_price' WHERE meta_key = '_sale_price'
                            AND post_id = {$arrVar['variation_id']}";
                        if(!$wpdb->query($strUpdateKey)) {
                            update_post_meta($productId, '_wpcd_sale_price', $newPrice);
                        }
                    }

                    // Get all categories that product belongs to
                    $arrCats = array();
                    $strCats = $arrPro->selected_cats;
                    if(isset($strCats) && !empty($strCats)) {
                        $arrCats = explode(',', $strCats);
                    }
                    $arrCats[] = $reqCatId;                
                    update_post_meta($arrVar['variation_id'], '_wpcd_cats', implode(',', array_unique($arrCats)));

                    // Update prices
                    $fltRegularPrice = $arrVar['regular_price'];
                    $discountAmount = get_discount_amount($arrCategoryPrice, $arrCats);
                    $newPrice = $fltRegularPrice - $discountAmount;

                    update_post_meta($arrVar['variation_id'], '_price', $newPrice); 
                    update_post_meta($arrVar['variation_id'], '_sale_price', $newPrice);
                }
            }
        }
        $arrCategoryPrice[$reqCatId] = array('type' => 'Fixed Amount', 'value' => $_POST['amount'], 'isActive' => $_POST['active']);
        update_option('wpcd_category_discount', serialize($arrCategoryPrice));
        die();
    }
}

function get_discount_amount($arrCatDetails, $arrSelectedCats) {

    $arrSelectedAmount = array();
    
    foreach($arrCatDetails AS $key => $arrVal) {

        if(isset($arrVal['isActive']) && !empty($arrVal['isActive']) && $arrVal['isActive'] == 'true' && in_array($key, $arrSelectedCats)) {
            $arrSelectedAmount[] = $arrVal['value'];
        }
    }
    
    return count($arrSelectedAmount) > 1?max($arrSelectedAmount):$arrSelectedAmount[0];
}

add_filter( 'woocommerce_get_variation_prices_hash', 'add_user_id_to_woocommerce_get_variation_prices_hash' );

function add_user_id_to_woocommerce_get_variation_prices_hash( $hash ) {
  $hash[] = rand();
  return $hash;
}

function wpcd_get_term_parents( $id, $taxonomy, $link = false, $separator = ' >> ', $nicename = false, $visited = array() ) {
    $chain = '';
    $parent = get_term( $id, $taxonomy );
    if ( is_wp_error( $parent ) )
            return $parent;

    if ( $nicename ) {
            $name = $parent->slug;
    } else {
            $name = $parent->name;
    }

    if ( $parent->parent && ( $parent->parent != $parent->term_id ) && !in_array( $parent->parent, $visited ) ) {
            $visited[] = $parent->parent;
            $chain .= wpcd_get_term_parents( $parent->parent, $taxonomy, $link, $separator, $nicename, $visited );
    }

    if ( $link ) {
            $chain .= '<a href="' . get_term_link( $parent, $taxonomy ) . '" title="' . esc_attr( sprintf( _e( "View all posts in %s" ), $parent->name ) ) . '">'.$parent->name.'</a>' . $separator;
    } else {
            $chain .= $name.$separator;
    }
    return $chain;
}

add_filter( 'admin_footer_text', 'wpcd_admin_footer_text', 1 );
function wpcd_admin_footer_text( $footer_text ) {
    if ( ! current_user_can( 'manage_woocommerce' ) || ! function_exists( 'wc_get_screen_ids' ) ) {
        return $footer_text;
    }
    $current_screen = get_current_screen();
    
    // Check to make sure we're on a discount admin page.
    if ( isset( $current_screen->id ) && $current_screen->id == 'woocommerce_page_wpcd-category-discount' ) {
        
        /* translators: %s: five stars */
        $footer_text = sprintf( __( 'For support, write us at : info@wooextend.com and if you like <strong>Woo Product Category Discount</strong> please leave us a %s rating. A huge thanks in advance!', 'woocommerce' ), '<a href="https://wordpress.org/support/plugin/woo-product-category-discount/reviews?rate=5#new-post" target="_blank" class="wc-rating-link" data-rated="' . esc_attr__( 'Thanks :)', 'woocommerce' ) . '">&#9733;&#9733;&#9733;&#9733;&#9733;</a>' );
        wc_enqueue_js( "
            jQuery( 'a.wc-rating-link' ).click( function() {
                jQuery.post( '" . WC()->ajax_url() . "', { action: 'woocommerce_rated' } );
                jQuery( this ).parent().text( jQuery( this ).data( 'rated' ) );
            });
        " );
        
    }

    return $footer_text;
}
?>