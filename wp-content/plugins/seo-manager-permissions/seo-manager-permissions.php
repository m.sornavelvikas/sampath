<?php # -*- coding: utf-8 -*-

/**
 * Plugin Name:  Seo Manager Permissions
 * Description:  Plugin permissions assigned for Seo Manager Permissions
 * Author:       Siva
 * Version:      1.0.0
 * Text Domain:  seo-manager-permissions
 * License:      GPLv2+
 * License URI:  LICENSE
 */


 
function redirection_seo_role() {
    return "edit_private_posts";
}

add_filter('redirection_role', 'redirection_seo_role');

add_filter('insr-capability', 'redirection_seo_role');

add_filter('search_replace_access_capability', 'redirection_seo_role');
