<?php
/**
 * This file adds the Landing template to the Beautique Pro Theme.
 *
 * @author Heather Jones
 * @package Beautique Pro
 * @subpackage Customizations
 */

/*
Template Name: Landing 2
*/
  
?>
<head>
<link rel="stylesheet" id="beautique-pro-theme-css" href="https://sampat-8fcqyfvxkvyhrt.netdna-ssl.com/wp-content/themes/beautique/style.css?ver=1494178224" type="text/css" media="all">
</head>
<body>
<div class="site-inner">
<div class="wrapper"style="    text-align: center;"><img src="https://sampat-8fcqyfvxkvyhrt.netdna-ssl.com/wp-content/uploads/2017/04/SampatJewellers-logo.png"/></div>
<div class="one-third first">
<div class="wrapper"></br><h2>Jewel: Navaratna Flexible Bracelet</h2>
Gold: 18K Yellow Gold</br>
Diamond Weight: 0.10 carat(s)</br>
Diamond Cut: Excellent, Full Facet for maximum sparkle</br>
Diamond Clarity: VS or higher</br>
Diamond Color: G or higher</br>
Gemstones: Natural, real gems from Mother Earth</br></br></br></div>
<h2>Price: $2100 USD</h2>
<form action="https://sampatjewelers.com/stripe/process.php" method="post" class="buy-form"> 

        <input 
            type="submit" 
            value="CLICK HERE TO BUY NOW"
            data-key="pk_live_QqWMxpZZJ3LaTusioob0hexs" 
            data-amount="210000"
            data-currency="usd"
            data-name="Sampat Jewellers Inc."
            data-description="20% Off Fine Jewelry for 5 Years" 
data-image="https://sampatjewelers.com/wp-content/uploads/2017/02/logo-regular_Oct2016-copy-1-1.jpg"
        />

        <script src="https://checkout.stripe.com/v2/checkout.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
        <script>
        $(document).ready(function() {
            $(':submit').on('click', function(event) {
                event.preventDefault();
                var $button = $(this),
                    $form = $button.parents('form');
                var opts = $.extend({}, $button.data(), {
                    token: function(result) {
                        $form.append($('<input>').attr({ type: 'hidden', name: 'stripeToken', value: result.id })).submit();
                    }
                });
                StripeCheckout.open(opts);
            });
        });
        </script>
</form>
</div>
<div class="one-half" style="float:right;" >
<img src="https://sampatjewelers.com/wp-content/uploads/2017/05/Navaratna-Flexible-Bracelet-Sampat-Jewellers-3-1.jpg"/>
</div>
<div class="two-thirds first">
<h3>A lil bit more about Navaratna Bracelet:</h3>
<p>The Navaratna Ripple combines modern design with the century old belief in the power of gems on human prosperity and well-being. </p>
<p>This trendy Navaratna ring is made of concentric rows of diamonds around a large center ruby on solid gold. The power of Navaratna jewelry comes from the natural and real gems that are flawless or near flawless. </p>
<p>Learn more about us and how we�ve been serving our clients since 1977. </p>
<p>Want your own unique piece? No problem.</p>
<p>Each piece is handmade with love at our studio in Mumbai. All pieces can be customized in 14K and 18K yellow, white and rose gold. </p>

<p>Have more questions?</p>
<p>Drop me an email at contact@sampatjewellers.com or call me at +1-408-703-1318. </p>

</div>
<div class="one-fourth">
<h2>Customer Testimonial</h2>
</div>
<div class="clearfix"></div>
</div>
</body>