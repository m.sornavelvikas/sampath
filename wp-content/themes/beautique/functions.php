<?php

 

//upload svg
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');


function defer_parsing_of_js ( $url ) {
if ( FALSE === strpos( $url, '.js' ) ) return $url;
if ( strpos( $url, 'jquery.js' ) ) return $url;
return "$url' defer ";
}
//add_filter( 'clean_url', 'defer_parsing_of_js', 11, 1 );


//*product feed start
function displaydate(){
     return date('F j, Y', strtotime('+14 day'));
}
add_shortcode( 'date', 'displaydate' );
 

//Define the product feed php page  
function products_feed_rss2() {  
 $rss_template = get_template_directory() . '/product-feed.php';  
 load_template ( $rss_template );  
}  

//Add the product feed RSS  
add_action('do_feed_products', 'products_feed_rss2', 10, 1);  
remove_action( 'woocommerce_checkout_order_review','woocommerce_checkout_payment', 20 );
add_action( 'woocommerce_after_order_notes', 'woocommerce_checkout_payment', 20 );
add_action( 'woocommerce_checkout_order_review','woocommerce_checkout_coupon', 20 );
//Update the Rerewrite rules  
add_action('init', 'my_add_product_feed');  

//function to add the rewrite rules  
function my_rewrite_product_rules( $wp_rewrite ) {  
 $new_rules = array( 'feed/(.+)' => 'index.php?feed='.$wp_rewrite->preg_index(1)  );  
 $wp_rewrite->rules = $new_rules + $wp_rewrite->rules;  
}  
   
//add the rewrite rule  
function my_add_product_feed( ) {  
 global $wp_rewrite;  
 add_action('generate_rewrite_rules', 'my_rewrite_product_rules');  
 $wp_rewrite->flush_rules();  
}  


//* product feed end



//* Start the engine
include_once( get_template_directory() . '/lib/init.php' );

//* Set Localization (do not remove)
load_child_theme_textdomain( 'beautique', apply_filters( 'child_theme_textdomain', get_stylesheet_directory() . '/languages', 'beautique' ) );

//* Child theme (do not remove)
define( 'CHILD_THEME_NAME', __( 'Beautique Pro Theme', 'beautique' ) );
define( 'CHILD_THEME_URL', 'http://my.studiopress.com/themes/beautique/' );
define( 'CHILD_THEME_VERSION', '3.0' );

//* Enqueue Google Fonts and JS script
add_action( 'wp_enqueue_scripts', 'beautique_enqueue_scripts' );
function beautique_enqueue_scripts() {

 	wp_enqueue_style( 'google-fonts', '//fonts.googleapis.com/css?family=Roboto+Slab:100,300,400,700|Karla:300,400,500,900', array(), CHILD_THEME_VERSION );

	wp_enqueue_script( 'beautique-entry-date', get_bloginfo( 'stylesheet_directory' ) . '/js/entry-date.js', array( 'jquery' ), '1.0.0' );
	wp_enqueue_script( 'beautique-responsive-menu', get_bloginfo( 'stylesheet_directory' ) . '/js/responsive-menu.js', array( 'jquery' ), '1.0.0' );

}

//* Add HTML5 markup structure
add_theme_support( 'html5' );

//* Add viewport meta tag for mobile browsers
add_theme_support( 'genesis-responsive-viewport' );

//* Add new image sizes
add_image_size( 'home-middle', 360, 200, true );
add_image_size( 'home-top', 750, 420, true );
add_image_size( 'sidebar-thumbnail', 100, 100, true );
add_image_size( 'slider', 300, 200, true );

//* Add support for additional color styles
add_theme_support( 'genesis-style-selector', array(
	'beautique-pro-blue'   => __( 'Blue Lagoon', 'beautique' ),
	'beautique-pro-pink'  => __( 'Pretty in Pink', 'beautique' ),
	'beautique-pro-purple'  => __( 'Purple Passion', 'beautique' 

),
	'beautique-pro-neutral' => __( 'Neutral', 'beautique' ),
) );

//* Customize the Credits
add_filter('genesis_footer_creds_text', 'footer_creds_filter');
function footer_creds_filter( $creds ) {
	$creds = 'Copyright [footer_copyright] <a href="http://vivalaviolette.com/genesis-themes/">Beautique Theme</a> by <a href="http://vivalaviolette.com">Viva la Violette</a>';
	return $creds;
}

//* Add support for Woocommerce
add_theme_support( 'genesis-connect-woocommerce' );


//* Customize search form input box text
add_filter( 'genesis_search_text', 'beautique_search_text' );
function beautique_search_text( $text ) {

	return esc_attr( __( 'Search the site ...', 'beautique' ) );
	
}

//* Modify the size of the Gravatar in the author box
add_filter( 'genesis_author_box_gravatar_size', 

'beautique_author_box_gravatar' );
function beautique_author_box_gravatar( $size ) {

	return 140;

}

// Enable shortcodes in widgets
add_filter('widget_text', 'do_shortcode');


//* Modify the size of the Gravatar in the entry comments
add_filter( 'genesis_comment_list_args', 

'beautique_comments_gravatar' );
function beautique_comments_gravatar( $args ) {

	$args['avatar_size'] = 100;
	return $args;

}

//* Remove entry meta in entry footer
add_action( 'genesis_before_entry', 'beautique_remove_entry_meta' );
function beautique_remove_entry_meta() {
	
	//* Remove if not single post
	if ( ! is_single() ) {
		remove_action( 'genesis_entry_footer', 'genesis_entry_footer_markup_open', 5 );
		remove_action( 'genesis_entry_footer', 'genesis_post_meta' );
		remove_action( 'genesis_entry_footer', 'genesis_entry_footer_markup_close', 15 );
	}

}

//* Hooks after-entry widget area to single posts
add_action( 'genesis_entry_footer', 'beautique_after_entry_widget'  ); 
function beautique_after_entry_widget() {

    if ( ! is_singular( 'post' ) )
    	return;

    genesis_widget_area( 'after-entry', array(
		'before' => '<div class="after-entry widget-area"><div class="wrap">',
		'after'  => '</div></div>',
    ) );

}

//* Add support for 3-column footer widgets
add_theme_support( 'genesis-footer-widgets', 3 );

add_action( 'genesis_before_header', 'top_bar' );

//* Add top bar above header.
function top_bar() {
	echo '<div class="top-bar"><div class="wrap">';
	genesis_widget_area( 'top-bar-left', array(
	'before' => '<div class="top-bar-left">',
	'after' => '</div>',
) );

	genesis_widget_area( 'top-bar-right', array(
	'before' => '<div class="top-bar-right">',
	'after' => '</div>',
) );
	echo '</div></div>';
}

//* Register widget areas
genesis_register_sidebar( array(
	'id'          => 'home-top',
	'name'        => __( 'Home - Top', 'beautique' ),
	'description' => __( 'This is the top section of the homepage.', 'beautique' ),
) );
genesis_register_sidebar( array(
	'id'          => 'home-middle',
	'name'        => __( 'Home - Middle', 'beautique' ),
	'description' => __( 'This is the middle section of the homepage.', 'beautique' ),
) );
genesis_register_sidebar( array(
	'id'          => 'home-bottom',
	'name'        => __( 'Home - Bottom', 'beautique' ),
	'description' => __( 'This is the bottom section of the homepage.', 'beautique' ),
) );
genesis_register_sidebar( array(
	'id'          => 'after-entry',
	'name'        => __( 'After Entry', 'beautique' ),
	'description' => __( 'This is the after entry section.', 'beautique' ),
) );
genesis_register_sidebar( array(
	'id' => 'top-bar-left',
	'name' => __( 'Top Bar Left', 'theme-prefix' ),
	'description' => __( 'This is the left top bar above the header.', 'theme-prefix' ),
) );
genesis_register_sidebar( array(
	'id' => 'top-bar-right',
	'name' => __( 'Top Bar Right', 'theme-prefix' ),
	'description' => __( 'This is the right top bar above the header.', 'theme-prefix' ),
) );

//if (is_product()) {
//}

remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_title', 5);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20);


//do_action('wc_custom_sidebar');

add_action('wc_custom_sidebar', 'woocommerce_template_single_title', 5);
add_action('wc_custom_sidebar', 'woocommerce_template_single_add_to_cart', 30);


add_action('wc_custom_product_summary_price', 'woocommerce_template_single_price', 10);
add_action('wc_custom_product_summary_desc', 'woocommerce_template_single_excerpt', 20);

add_action('wc_custom_product_summary', 'wp_custom_product_summary_method');

function wp_custom_product_summary_method () {

	do_action('wc_custom_product_summary_desc');

	do_action('wc_custom_product_summary_price');
        $img_url = $image = wp_get_attachment_image_src( get_post_thumbnail_id( $loop->post->ID ), 'single-post-thumbnail' );
	echo '<div class="extraLinks">
			<div class="linkContainer">
			<!--	<h2><a href="javascript:;" 

id="saveThisDesign">Save this Design</a></h2> -->
			</div>
			
			<div class="linkContainer addtocart">
				<h2><a href="javascript:;" class="triggerAddToCart">Add to Cart</a></h2>
			</div>
			<div class="shipping-details">
			<p>Order now for delivery by '.displaydate().'</p>
			<p>Free Shipping and Free Returns</p></div>
                        <div class="linkContainer">
<div class="product-share">
  <div class="social-share-btn"><a href="http://www.facebook.com/sharer/sharer.php?u='.get_the_permalink().'&title='.get_the_title().'"><span class="menu-icon fa fa-facebook"></span></a></div>
   <div class="social-share-btn"><a href="http://pinterest.com/pin/create/bookmarklet/?media='.$image[0].'&url='.get_the_permalink().'&is_video=false&description='.get_the_title().'"><span class="menu-icon fa fa-pinterest"></span></a></div>
   <div class="ezcol ezcol-one-quarter"></div>
<div class="ezcol ezcol-one-quarter ezcol-last"></div></div></div>
		  </div>';

}
add_action('wp_footer','customFooterScript');
function customFooterScript(){
?>
<script>
jQuery(function() {
  jQuery(document).on('click','#saveThisDesign',function() {
    var bookmarkURL = window.location.href;
    var bookmarkTitle = document.title;

    if ('addToHomescreen' in window && addToHomescreen.isCompatible) 
{
      // Mobile browsers
      addToHomescreen({ autostart: false, startDelay: 0 }).show(true);
    } else if (window.sidebar && window.sidebar.addPanel) {
      // Firefox <=22
      window.sidebar.addPanel(bookmarkTitle, bookmarkURL, '');
    } else if ((window.sidebar && /Firefox/i.test(navigator.userAgent)) || (window.opera && window.print)) {
      // Firefox 23+ and Opera <=14
      jQuery(this).attr({
        href: bookmarkURL,
        title: bookmarkTitle,
        rel: 'sidebar'
      }).off(e);
      return true;
    } else if (window.external && ('AddFavorite' in window.external)) 

{
      // IE Favorites
      window.external.AddFavorite(bookmarkURL, bookmarkTitle);
    } else {
      // Other browsers (mainly WebKit & Blink - Safari, Chrome, Opera 15+)
      alert('Press ' + (/Mac/i.test(navigator.userAgent) ? 'Cmd' : 'Ctrl') + '+D to bookmark this page.');
    }

    return false;
  });
});
</script>
<?php
}

// Custom Stylesheet ADD to Head Section - Themes Using Action Hooks

function add_custom_stylesheet() {
    echo "\n";
    echo '<link rel="stylesheet" href="https://sampatjewelers.com/includes/css/custom_plugins011217.css" type="text/css" media="screen" />';
    echo '<link rel="icon" href="https://sampatjewelers.com/includes/image/favicon.ico" />';
    echo "\n";
}
//add_action('wp_head', 'add_custom_stylesheet');


function remove_head_scripts(){
remove_action('wp_head', 'wp_print_scripts');
remove_action('wp_head', 'wp_print_head_scripts', 9);
remove_action('wp_head', 'wp_enqueue_scripts', 1);

remove_action('wp_footer', 'wp_print_scripts', 5);
remove_action('wp_footer', 'wp_enqueue_scripts', 5);
remove_action('wp_footer', 'wp_print_head_scripts', 5);
}
//add_action('wp_enqueue_scripts', 'remove_head_scripts');
 
//delete wp-emoji.js
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

// Disable CSS Site-Wide Using CSS Script Handle

//add_action( 'wp_print_styles', 'my_deregister_styles', 100 );

function my_deregister_styles() 
	{
	wp_deregister_style( 'genesis-simple-share-plugin-css' );
	wp_deregister_style( 'bne-testimonials-css' );
	wp_deregister_style( 'contact-form-7' );
	wp_deregister_style( 'direct-stripe-style' );
	wp_deregister_style( 'sb_instagram_styles' );
	wp_deregister_style( 'sb_instagram_icons' );
	wp_deregister_style( 'woo-feed-css' );
	wp_deregister_style( 'fontawesome' );
	wp_deregister_style('omsc-shortcodes');
	wp_deregister_style('omsc-shortcodes-mobile');
	wp_deregister_style('rp-public-styles');
	wp_deregister_style('nb-styles');
	wp_deregister_style('woocommerce-general');
	wp_deregister_style('swatches-and-photos');
	wp_deregister_style('simple-social-icons-font');
	wp_deregister_style('genesis-simple-share-genericons-css');
	wp_deregister_style('woo-feed');
	wp_deregister_style('woocommerce-layout');
	wp_deregister_style('omsc-shortcodes-tablet');
	wp_deregister_style('style');
	wp_deregister_style('ywzm-prettyPhoto');
	wp_deregister_style('ywzm-magnifier');
	wp_deregister_style('ywzm_frontend');
	wp_deregister_style('woocommerce-smallscreen');
	
	
	}
	
	

	add_filter( 'loop_shop_per_page', 'new_loop_shop_per_page', 20 );
function new_loop_shop_per_page( $cols ) {
  // $cols contains the current number of products per page based on the value stored on Options -> Reading
  // Return the number of products you wanna show per page.
  $cols = 30;
  return $cols;
}




//add_action( 'woocommerce_after_single_product_summary', 'comments_template', 50 );

function wpdocs_dequeue_script() {
   if ( class_exists('q2w3_fixed_widget', false) ) {
	global $wp_scripts;
	$wp_scripts->registered['q2w3_fixed_widget']->src = get_stylesheet_directory_uri() . '/js/q2w3-fixed-widget.js';
   }
}
add_action( 'wp_print_scripts', 'wpdocs_dequeue_script', 100 );

