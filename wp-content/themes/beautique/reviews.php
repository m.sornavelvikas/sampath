<?php
/**
* Template Name: reviews
* Description: Used as a page template to show page contents, followed by a loop 
* through the "Genesis Office Hours" category
*/
// Add our custom loop
add_action( 'genesis_loop', 'cd_goh_loop' );
 
function cd_goh_loop() {

 echo'<div style="    background-size: cover !important;    height: 500px; margin-bottom:30px;    background: url(https://sampatjewelers.com/wp-content/uploads/2017/11/Review-cover.jpg) no-repeat;"> <div class="wrap">
<div class="ezcol ezcol-one-half"><h1>Real Customer Stories</h1>
<span style="font-size: 16px !important;">Our clients share their photos and stories of how our jewels have made them happy. To share photos or stories of your unforgettable memories, drop us a line at contact@sampatjewellers.com </span></div></div></div>
 <div class="wrap"><div class="masonry-loop">';
 $args = array( 
 'post_type' => 'review',  
 'posts_per_page'=> '60',
 );
 $loop = new WP_Query( $args );
 if( $loop->have_posts() ) {
 // loop through posts
 while( $loop->have_posts() ): $loop->the_post();
 echo'<article class="masonry-entry">';
if ( has_post_thumbnail() ) :
 echo' <div class="masonry-thumbnail">  ';
 the_post_thumbnail('masonry-thumb'); 
  echo'</div> ';
  endif; 
  echo'  <div class="masonry-details">
         <div class="masonry-post-excerpt">';
		 the_content();
  echo'     </div> 
    </div> 
</article> ';

endwhile;
 }
 wp_reset_postdata();
}
  echo'     </div> 
</div> ';

 genesis();  