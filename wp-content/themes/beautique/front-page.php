<?php
/**
 * This file adds the Home Page to the Beautique Pro Child Theme.
 *
 * @author Heather Jones
 * @package Beautique Pro
 * @subpackage Customizations
 */

add_action( 'genesis_meta', 'beautique_home_genesis_meta' );
/**
 * Add widget support for homepage. If no widgets active, display the default loop.
 *
 */
function beautique_home_genesis_meta() {

	if ( is_active_sidebar( 'home-top' ) || is_active_sidebar( 'home-middle' ) || is_active_sidebar( 'home-bottom' ) ) {

		// Force content-sidebar layout setting
		add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_sidebar_content' );

		// Add beautique-home body class
		add_filter( 'body_class', 'beautique_body_class' );

		// Remove the default Genesis loop
		remove_action( 'genesis_loop', 'genesis_do_loop' );

		// Add homepage widgets
		add_action( 'genesis_loop', 'beautique_homepage_widgets' );

	}
}

function beautique_body_class( $classes ) {

	$classes[] = 'beautique-home';
	return $classes;
	
}

function beautique_homepage_widgets() {

	genesis_widget_area( 'home-top', array(
		'before' => '<div class="home-top widget-area">',
		'after'  => '</div>',
	) );

	genesis_widget_area( 'home-middle', array(
		'before' => '<div class="home-middle widget-area">',
		'after'  => '</div>',
	) );

	genesis_widget_area( 'home-bottom', array(
		'before' => '<div class="home-bottom widget-area">',
		'after'  => '</div>',
	) );

}

genesis();