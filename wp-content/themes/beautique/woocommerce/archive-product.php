<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>

	<?php
		/**
		 * woocommerce_before_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20
		 */
		do_action( 'woocommerce_before_main_content' );
	?>

		<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>

			<h1 class="page-title test"><?php woocommerce_page_title(); ?></h1>

		<?php endif; ?>

		<?php
			/**
			 * woocommerce_archive_description hook.
			 *
			 * @hooked woocommerce_taxonomy_archive_description - 10
			 * @hooked woocommerce_product_archive_description - 10
			 */
			do_action( 'woocommerce_archive_description' );
		?>

		<?php if ( have_posts() ) : ?>

			<?php
				/**
				 * woocommerce_before_shop_loop hook.
				 *
				 * @hooked woocommerce_result_count - 20
				 * @hooked woocommerce_catalog_ordering - 30
				 */
				do_action( 'woocommerce_before_shop_loop' );
			?>

			<?php woocommerce_product_loop_start(); ?>

				<?php woocommerce_product_subcategories(); ?>

				<?php while ( have_posts() ) : the_post(); ?>

					<?php wc_get_template_part( 'content', 'product' ); ?>

				<?php endwhile; // end of the loop. ?>

			<?php woocommerce_product_loop_end(); ?>

			<?php
				/**
				 * woocommerce_after_shop_loop hook.
				 *
				 * @hooked woocommerce_pagination - 10
				 */
				do_action( 'woocommerce_after_shop_loop' );
			?>

		<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

			<?php wc_get_template( 'loop/no-products-found.php' ); ?>

		<?php endif; ?>

	<?php
		/**
		 * woocommerce_after_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
		do_action( 'woocommerce_after_main_content' );
	?>
	<?php if ( is_product_category( 'mangalsutra' ) ) {
    ?>
<script src="https://fast.wistia.com/embed/medias/yp9i6a2jqs.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><div class="wistia_responsive_padding" style="padding:45.25% 0 0 0;position:relative;"><div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;padding: 0 10%; width: 100%;"><div class="wistia_embed wistia_async_yp9i6a2jqs videoFoam=true" style="height:100%;width:100%">&nbsp;</div></div></div>
	<?php }
  elseif ( is_product_category( 'diamond-mangalsutras' ) ){?><script src="https://fast.wistia.com/embed/medias/yp9i6a2jqs.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><div class="wistia_responsive_padding" style="padding:45.25% 0 0 0;position:relative;"><div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;padding: 0 10%; width: 100%;"><div class="wistia_embed wistia_async_yp9i6a2jqs videoFoam=true" style="height:100%;width:100%">&nbsp;</div></div></div><?php }	else { } ?>
	
	<?php
		/**
		 * woocommerce_sidebar hook.
		 *
		 * @hooked woocommerce_get_sidebar - 10
		 */
		do_action( 'woocommerce_sidebar' );
	?>
<script> window.onload = function(){ 
var customersuppportpopup = document.getElementById('customer-suppport'); 
var btn = document.getElementById("customer-suppport-btn");
 var span = document.getElementsByClassName("closecs")[0]; 
btn.onclick = function() {
    customersuppportpopup.style.display = "block";
} 
span.onclick = function() {
    customersuppportpopup.style.display = "none";
} 
window.onclick = function(event) {
    if (event.target == customersuppportpopup) {
        customersuppportpopup.style.display = "none";
    }
}};
</script>

<div class="customer-feedback">
 For more information contact <button id="customer-suppport-btn">Customer Support</button> </div>

<div id="customer-suppport" class="customersuppportpopup"> 
  <div class="customer-suppport-content">
    <span class="closecs">&times;</span><div class="cs-popup-inner"><div class="ezcol ezcol-one-half"><h3>Need Assitance?</h3><p>Our non-commissioned jewelry experts are here to help, and will respond quickly to any questions you may have! Fill out the email form or contact us using one of the other methods below.</p><?php echo do_shortcode( '[contact-form-7 id="4050" title="contact popup"]' ); ?></div>
<div class="ezcol ezcol-one-half ezcol-last">
<div class="row">
<div class="ezcol ezcol-one-fifth"><img src="https://sampat-8fcqyfvxkvyhrt.netdna-ssl.com/wp-content/uploads/2017/04/talking-by-phone-auricular-symbol-with-speech-bubble.png"/></div>
<div class="ezcol ezcol-four-fifth ezcol-last"><p><em><strong>HELLO. (408)-703-1318</strong></em><br>We have real live human beings answering phones from 8-7pm PST</p></div>
</div>
<div class="row">
<div class="ezcol ezcol-one-fifth"><img src="https://sampat-8fcqyfvxkvyhrt.netdna-ssl.com/wp-content/uploads/2017/04/opened-email-envelope.png"/></div>
<div class="ezcol ezcol-four-fifth ezcol-last"><p><em><strong>CONTACT@SAMPATJEWELLERS.COM</strong></em><br>Email contact@sampatjewellers.com, and the same real live human beings will get back to you within 24 hours</p></div>
</div>
<div class="row">
<div class="ezcol ezcol-one-fifth"><img src="https://sampat-8fcqyfvxkvyhrt.netdna-ssl.com/wp-content/uploads/2017/04/icon.png"/></div>
<div class="ezcol ezcol-four-fifth ezcol-last"><p><a href="https://calendly.com/jinalsampat/30min" style="color: #434444; font-weight: bold;"><em><strong>VIDEO APPOINTMENT</strong></em></a><br> Have a unique jewelry piece in mind? Schedule a complimentary 30-minute Skype call with our Design Lab specialist. <a href="" onclick="Calendly.showPopupWidget('https://calendly.com/jinalsampat/30min');return false;">Click here to Schedule</a> </p></div>
</div>
</div>
</div>

</div>
  </div>

<?php get_footer( 'shop' ); ?>