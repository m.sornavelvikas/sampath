<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>

	<?php
		/**
		 * woocommerce_before_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20
		 */
		do_action( 'woocommerce_before_main_content' );
	?>

		<?php while ( have_posts() ) : the_post(); ?>

			<?php wc_get_template_part( 'content', 'single-product' ); ?>

		<?php endwhile; // end of the loop. ?>

	<?php
		/**
		 * woocommerce_after_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
		do_action( 'woocommerce_after_main_content' );
	?>

	<?php
		/**
		 * woocommerce_sidebar hook.
		 *
		 * @hooked woocommerce_get_sidebar - 10
		 */
		//do_action( 'woocommerce_sidebar' );
	?>
		<!-- <aside class="product-detail sidebar sidebar-primary widget-area">
			<?php // do_action( 'wc_custom_sidebar' ); ?>
		</aside> -->
<div class="ezcol pro-description" style="float:left;width:100%;">
<div class="ezcol ezcol ezcol-one-half"><?php the_content(); ?></div>
<div class="ezcol ezcol ezcol-one-half-last"><h3>Our Diamonds</h3>
<p>G color or higher, VS Clarity or higher and Excellent Cut diamonds.</p>
<p>Full cut diamonds with 56-57 facets for maximum sparkle. No single cut diamonds with 16-17 facets.</p>
<p>Diamonds are real, natural and product of Mother Earth. No CZ or synthetic diamonds.</p>
<p>Diamonds rank very high on the Gemological Institute of America (GIA) diamond quality scale.</p>
<p>We take pride in our diamonds. We ensure consistent quality and diamond characteristics.</p></div>
</div>
</div> <!--closing class="site-inner"-->
<div class="ezcol satisfaction" style="float:left;width:100%;background-color: #d9ede6;text-align: center;padding: 25px 0 0 0;">
<h2>100% Satisfaction Gaurentee</h2>
<p>If you’re not 100% satisfied, we promise to make everything right.</br>If you have any questions, before or after purchasing, please let us know!</p>
<p>(408) 703-1318	|	contact@sampatjewellers.com</p>
</div>
<div class="ezcol free-shipping" style="float:left;width:100%;text-align: center;padding: 25px 0 0 0;">
<h2>Free Shipping within USA and India</h2>
<p>Risk-Free 14-Day Return</br>Complimentary Gift Wrapping</br>Delivered by <?php echo ''.displaydate().'' ?></p> 
</div>
<div class="site-inner">
<div class="container">
<?php 
		do_action( 'woocommerce_after_single_product_summary' );
		?>
		</div>
		</div>
<div class="ezcol"><div class="sampat-offer" style="    background-image: url('https://sampat-8fcqyfvxkvyhrt.netdna-ssl.com/wp-content/uploads/2017/07/Your-Modern-Indian-Jeweler_4.jpg');    background-size: cover;    text-align: center;    padding: 79px 77px;    background-repeat: no-repeat;    width: 100%;    float: left;    ">
<h2 style="    font-weight: bold;font-size: 33px;">Handmade, Versatile Fine Jewelry</h2> 
<p style="font-size: 17px;margin: 0px 12%;">We create heartwarming fine jewelry that you’ll want to wear everyday and not store in the bank safe. Our jewels are handmade by artisans from West Bengal, India. Through our pieces we want you to experience centuries of Indian culture and heritage. Each piece is designed for versatility, elegance and beauty.</p>
</div>
</div>


<?php get_footer( 'shop' ); ?>
