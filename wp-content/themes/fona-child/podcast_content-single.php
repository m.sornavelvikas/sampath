<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package     Zoo Theme
 * @version     1.0.0
 * @author      Zootemplate
 * @link        http://www.zootemplate.com
 * @copyright   Copyright (c) 2017 Zootemplate
 * @license     GPL v2
 */
?>
<?php 
$id = array();
$id[] = get_the_ID();
?>
    <article id="post-<?php the_ID(); ?>" <?php post_class('post-item single_podcast_page'); ?>>
	<?php
		/*
        <div class="header-post">
			<div class="container">
				<div class="wrap-header-post-info">
					<?php
					if (is_sticky()) {
						?>
						<span class="sticky-post-label"><?php echo esc_html__('Featured', 'fona') ?></span>
						<?php
					}
					the_title('<h1 class="title-detail">', '</h1>');
					?>
				</div>
				<?php  if (has_post_thumbnail()) :  ?>
					<div class=" media-block post-image single-image">
						<div class="podcast_border">
						<?php
							 $strFile = get_post_meta($post -> ID, $key = 'podcast_file', true);
						?>
						<img class="podcast_author_image <?php if (!empty($strFile)) { echo("url_present"); }?>" src="<?php echo $strFile; ?>" style="width:150px;">
						</div>
					</div>
				<?php endif; ?>
			</div>
        </div>
		*/
		?>
        <div class="container">
            <div class="wrap-post-content col-sm-9 col-xs-12">
                <div class="post-content">
                    <?php
                    the_content();
                    ?>
                </div>
                <?php
                //do not remove
                get_template_part('inc/templates/inpost', 'pagination');
                edit_post_link(esc_html__('Edit', 'fona'), '<span class="edit-link">', '</span>');
                //Allow custom below
                ?>
                <div class="bottom-post">
                    <?php
                    get_template_part('inc/templates/posts/single/tag');
                   // get_template_part('inc/templates/posts/single/share', 'post');
                    ?>
                </div>
            </div>
			<?php if ( is_active_sidebar( 'podcast_sidebar' ) ) : ?>
					<div id="primary-sidebar" class="podcast_sidebar primary-sidebar widget-area col-sm-3 col-xs-12" role="complementary">
						<div class="podcast_sidebar_fixed" >
							<?php dynamic_sidebar( 'podcast_sidebar' ); ?>
						</div>
					</div><!-- #primary-sidebar -->
			<?php endif; ?>
        </div>
        <div class="footer-post container">
            <?php
            get_template_part('inc/templates/posts/single/post', 'navigation');
            get_template_part('inc/templates/posts/single/about', 'author');
            get_template_part('inc/templates/posts/single/related', 'posts');
            ?>
        </div>
    </article>
<?php
if (comments_open() || get_comments_number()) :
    comments_template('', true);
endif;

$terms = get_the_terms( get_the_ID() , 'podcast_cat' );
if ( !empty( $terms ) ){
	// get the first term
	$term = array_shift( $terms );
	$current_term = $term->slug;
	
	$args = array(
		'post_type' => 'podcast',
		'post__not_in' =>$id,
		'posts_per_page' => 3,
		'tax_query' => array(
			array(
				'taxonomy' => 'podcast_cat',
				'field' => 'id',
				'terms' => $current_term,
			),
		),
	);
}
$query = new WP_Query($args);
if($query->have_posts()):
	?>
	<div class="podcast_cover_container_heading header_select_podcast">
		If you liked this episode… Then You'll Love These Too!	
	</div>
	<?php
	endif;
	?>
	<div class="podcast_cover_container">
		<div id="response" class="podcast_sub_cover clearfix">
			<?php
				$query = new WP_Query($args);
				if($query->have_posts()):
					while($query->have_posts()):$query->the_post();
						$link = get_post_permalink( get_the_ID() );
						?>
						<div class="category_filter_container">
							<?php if (has_post_thumbnail( get_the_ID()) ): ?>
								<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID()), 'single-post-thumbnail' ); ?>
								<a href="<?php echo($link); ?>" target="_blank">
									<div id="podcast-image-bg" style="background-image: url('<?php echo $image[0]; ?>')"></div>
								</a>
							<?php endif; ?>
							<a href="<?php echo($link); ?>" target="_blank"><h4 class="podcast_header"><?php echo($query->post->post_title); ?></h4></a>
						</div>

						<?php
					endwhile;
				endif;
			?>
		</div>
	</div>
	<?php


