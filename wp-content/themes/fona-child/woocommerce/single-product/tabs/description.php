<?php
/**
 * Description tab
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/tabs/description.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post;

$heading = esc_html( apply_filters( 'woocommerce_product_description_heading', __( 'Product Details', 'woocommerce' ) ) );

?>

<div class="product_details_container">
	<div class ="col-xs-12 col-sm-4">
		<?php 
			$get_data = get_post_meta(get_the_ID(), "mangalsutra_information", true); 
			if (!empty($get_data[0]['product_varient_label'])) {
			
		?>
			<h4 class="product_details_header">INFORMATION</h4>
			<?php 
			$get_data = get_post_meta(get_the_ID(), "mangalsutra_information", true); 
			foreach($get_data as $data){
				?>
				<div class ="product_spec_row <?php if($data["product_varient_popup"] != "" ) { echo('popup_icon_details popmake-'.$data["product_varient_popup"]); } ?>">
					<span class ="product_spec_left">
						<?php echo($data["product_varient_label"].': '); ?>
					</span>
					<span class ="product_spec_right">
						<?php echo($data["product_varient_value"]); ?>
					</span>
				</div>
				<?php
			}
		}
		?>
	</div>
	<div class ="col-xs-12 col-sm-4">
	<?php 
		$get_data = get_post_meta(get_the_ID(), "gemstone_information", true); 
		if (!empty($get_data[0]['product_varient_label'])) {
	?>
			<h4 class="product_details_header">GEMSTONE INFORMATION</h4>
			<?php 
			foreach($get_data as $data){
					?>
				<div class ="product_spec_row <?php if($data["product_varient_popup"] != "" ) { echo('popup_icon_details popmake-'.$data["product_varient_popup"]); } ?>">
					<span class ="product_spec_left">
						<?php echo($data["product_varient_label"].': '); ?>
					</span>
					<span class ="product_spec_right">
						<?php echo($data["product_varient_value"]); ?>
					</span>
				</div>
				<?php
			}
		}
		?>
	</div>
	<div class ="col-xs-12 col-sm-4">
	<?php 
		$get_data = get_post_meta(get_the_ID(), "luxuriously_handcraft", true); 
		if (!empty($get_data[0]['product_varient_value'])) {
	?>
			<h4 class="product_details_header">LUXURIOUSLY HANDCRAFTED</h4>
			<?php 
			$get_data = get_post_meta(get_the_ID(), "luxuriously_handcraft", true); 
			foreach($get_data as $data){
			$url = do_shortcode( $data["product_varient_label"] );
					?>
					
				<div class ="product_spec_row <?php if($data["product_varient_popup"] != "" ) { echo('popup_icon_details popmake-'.$data["product_varient_popup"]); } ?>">
					<img src="<?php echo $url; ?>">
					<span class ="product_spec_left">
						<?php echo($data["product_varient_value"]); ?>
					</span>
				</div>
				<?php
			}
		}
		?>
	</div>
</div>