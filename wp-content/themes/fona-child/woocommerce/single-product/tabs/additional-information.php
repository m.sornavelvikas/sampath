<?php
/**
 * Additional Information tab
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/tabs/additional-information.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author        WooThemes
 * @package       WooCommerce/Templates
 * @version       3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

$heading = esc_html( apply_filters( 'woocommerce_product_additional_information_heading', __( 'Additional information', 'woocommerce' ) ) );

?>

<?php 
$product_single_page = get_option( 'product_single_page_sam' );
    ?>
	<div class="single-shipping-container">
		<div class="single-shipping-image-container">
			<img src="<?php echo($product_single_page['shipping_details_image_url']) ?>" class="single-shipping-image">
		</div>
		<div class="single-shipping-text">
			<?php  echo apply_filters( 'meta_content_product_des', $product_single_page['shipping_details_text'] ); ?>
	</div>
</div>

<?php //do_action( 'woocommerce_product_additional_information', $product ); ?>
