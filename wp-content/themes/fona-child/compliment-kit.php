<?php
class compleimentkit
{
    /**
     * Holds the values to be used in the fields callbacks
     */
    private $options;

    /**
     * Start up
     */
    public function __construct()
    {
        add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
        add_action( 'admin_init', array( $this, 'page_init' ) );
    }

    /**
     * Add options page
     */
    public function add_plugin_page()
	
    {	
	add_menu_page('Single Product Page', 'Single Product Page Settings', 'administrator','product_single_page_settings_page',   array( $this, 'create_admin_page' ) ,'dashicons-clipboard' );

      
    }

    /**
     * Options page callback
     */
    public function create_admin_page()
    {
        // Set class property
        $this->options = get_option( 'product_single_page_sam' );
        ?>
        <div class="wrap">
            <h1>Single Product Page Settings</h1>
            <form method="post" action="options.php">
            <?php
                // This prints out all hidden setting fields
                settings_fields( 'product-single-page-settings-group' );
                 do_settings_sections( 'product_single_page_settings_page' );
                submit_button();
            ?>
            </form>
        </div>
        <?php
    }

    /**
     * Register and add settings
     */
    public function page_init()
    {        
        register_setting(
            'product-single-page-settings-group', // Option group
            'product_single_page_sam', // Option name
            array( $this, 'sanitize' ) // Sanitize
        );

        add_settings_section(
            'setting_section_id', // ID
            'Compliment kit', // Title
            array( $this, 'print_section_info' ), // Callback
            'product_single_page_settings_page' // Page
        );  


		add_settings_field(
            'upload_image_button', // ID
            'Compliment Kit Section iImage ', // Title 
            array( $this, 'image_upload_button' ), // Callback
            'product_single_page_settings_page', // Page
            'setting_section_id' // Section           
        );	
		add_settings_field(
            'compliment_kit_text', // ID
            'Compliment Kit Section Text ', // Title 
            array( $this, 'compliment_kit_text_sec' ), // Callback
            'product_single_page_settings_page', // Page
            'setting_section_id' // Section           
        );
		add_settings_section(
            'setting_section_id_shipping', // ID
            'Shipping Details', // Title
            array( $this, 'print_section_info_shipping' ), // Callback
            'product_single_page_settings_page' // Page
        );  


		add_settings_field(
            'upload_image_button_shipping', // ID
            'Shipping Details Section Image ', // Title 
            array( $this, 'image_upload_button_shipping' ), // Callback
            'product_single_page_settings_page', // Page
            'setting_section_id_shipping' // Section           
        );	
		add_settings_field(
            'shipping_details_text', // ID
            'Shipping Details Text ', // Title 
            array( $this, 'shipping_details_text' ), // Callback
            'product_single_page_settings_page', // Page
            'setting_section_id_shipping' // Section           
        );		

      
    }

    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize( $input )
    {
        $new_input = array();
        if( isset( $input['product_upload_image'] ) )
            $new_input['product_upload_image'] = sanitize_text_field( $input['product_upload_image'] );
		
		if( isset( $input['compliment_kit_text'] ) )
            $new_input['compliment_kit_text'] = $input['compliment_kit_text'];

        if( isset( $input['shipping_details_image_url'] ) )
            $new_input['shipping_details_image_url'] = sanitize_text_field( $input['shipping_details_image_url'] );
		
		if( isset( $input['shipping_details_text'] ) )
            $new_input['shipping_details_text'] = $input['shipping_details_text'];
		
        return $new_input;
    }

    /** 
     * Print the Section text
     */
    public function print_section_info()
    {
        print 'Edit your  details in your compliment product session in your single product page';
    }

    /** 
     * Get the settings option array and print one of its values
     */

	public function image_upload_button()
    {
        printf(
            '<img id="compliment-kit-image-preview" src="%s">
			<input type="button" value="Upload Image/Select Image" id="upload_image_button" name="product_single_page_sam[upload_image_button]" />
			<input type="hidden" type="text" size="36" id="product_upload_image" name="product_single_page_sam[product_upload_image]" value="%s" />',
            isset( $this->options['product_upload_image'] ) ? esc_attr( $this->options['product_upload_image']) : '',
			isset( $this->options['product_upload_image'] ) ? esc_attr( $this->options['product_upload_image']) : ''
        );
    }
	public function compliment_kit_text_sec()
    {
       $options = get_option( 'product_single_page_sam', array() );
		 $content = isset( $options['compliment_kit_text'] ) ?   $options['compliment_kit_text'] : false;
		 echo wp_editor( $content, 'compliment_kit_text', array('textarea_name' => 'product_single_page_sam[compliment_kit_text]', 'media_buttons' => false)  );
    }
	
	  public function print_section_info_shipping()
    {
        print 'Edit your  details in your shipping tab in your single product page';
    }
	public function image_upload_button_shipping()
    {
        printf(
            '<img id="shipping-details-preview" src="%s" height="250" width="350">
			<input type="button" value="Upload Image/Select Image" id="upload_image_button_shipping" name="product_single_page_sam[upload_image_button_shipping]" />
			<input type="hidden" type="text" size="36" id="shipping_details_image_url" name="product_single_page_sam[shipping_details_image_url]" value="%s" />',
            isset( $this->options['shipping_details_image_url'] ) ? esc_attr( $this->options['shipping_details_image_url']) : '',
			isset( $this->options['shipping_details_image_url'] ) ? esc_attr( $this->options['shipping_details_image_url']) : ''
        );
    }
	public function shipping_details_text()
    {
       $options = get_option( 'product_single_page_sam', array() );
		 $content = isset( $options['shipping_details_text'] ) ?   $options['shipping_details_text'] : false;
		 echo wp_editor( $content, 'shipping_details_text', array('textarea_name' => 'product_single_page_sam[shipping_details_text]', 'media_buttons' => false)  );
    }
}

if( is_admin() )
    $my_settings_page = new compleimentkit();