<?php
/**
 * Template Name: Full Width Page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
get_header();?>
    <main id="main" class="single-page">
        <div class="page_container">
            <?php while (have_posts()) : the_post();
                get_template_part('content', 'page');
                if (comments_open() || get_comments_number()) :
                    comments_template('', true);
                endif;
            endwhile; ?>
        </div>
    </main>
<?php
get_footer();