<?php
add_shortcode( 'reviews-page-template' , 'reviews_page_template' );
function reviews_page_template(){
	ob_start();
	?>
	<div class="wrap reviews-page-template">
		<div class="review_page_image_container clearfix">
			<div class="review_page_image">
				
				<h2> Customer Reviews </h2>
			</div>
			<?php $page_id=get_the_ID(); $feature_image_url=get_the_post_thumbnail_url( $page_id ); ?>
			<div class="review_page_image2" style="background-image: url('<?php echo $feature_image_url;  ?>');" >	
			</div>
		</div>
		<div class="review_content clearfix">
		<?php 
			$args = array( 
			 'post_type' => 'review',  
			 'posts_per_page'=> '4',
			 );
			$loop = new WP_Query( $args );
			if( $loop->have_posts() ) {
				 // loop through posts
				 while( $loop->have_posts() ): $loop->the_post();
				 ?>
				<div class="masonry-entry clearfix">
					<?php 
					if ( has_post_thumbnail() ) :
					?>
					<div class="masonry-thumbnail">
						<?php 
						the_post_thumbnail('masonry-thumb'); 
						?>
					</div>
					<?php 
					endif; 
					?>
					<div class="masonry-content">
						<div class="review_post_title"><?php the_title(); ?> </div>
						<div class="review_post_content"><?php the_content();?></div>
					</div>
				</div>
				<?php
				endwhile;
			}
			wp_reset_postdata();
		?>
		</div>
		<div class="loading_image_div">
			<img id="loading_image" src="<?php echo get_theme_file_uri().'/images/loading.gif'; ?>" alt="loading image">
		</div>
	</div>
	<?php
	$output = ob_get_contents();
	ob_end_clean();
	return $output;
}
?>
