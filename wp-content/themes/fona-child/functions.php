<?php
	/**
	 * Theme functions and definitions
	 *
	 * @link https://developer.wordpress.org/themes/basics/theme-functions/
	 */

add_action( 'init', 'dev_init' );
function dev_init() {
	if(isset($_GET['dev'])) {
		echo("<pre>");
		print_r(get_intermediate_image_sizes());
		die();
	}
}
include( get_stylesheet_directory() . '/reviews.php');
add_action( 'after_setup_theme', 'product_custom_image_sizes' );
function product_custom_image_sizes() {
    add_image_size( 'product-preview-medium', 330, 416, true ); // (cropped)
    add_image_size( 'product-preview-large', 660, 832, true ); // (cropped)
}

if (!function_exists('woocommerce_template_loop_product_thumbnail')) {
    function woocommerce_template_loop_product_thumbnail()
    {
        $zoo_img = get_post_thumbnail_id(get_the_ID());
        $zoo_attachments = get_attached_file($zoo_img);
        if (has_post_thumbnail() && $zoo_attachments) :
            $zoo_item = wp_get_attachment_image_src($zoo_img, 'shop_catalog');
            $zoo_img_url = $zoo_item[0];
            $zoo_width = $zoo_item[1];
            $zoo_height = $zoo_item[2];
            $resolution = $zoo_width / $zoo_height;
            $zoo_img_title = get_the_title($zoo_img);
            $zoo_img_srcset = wp_get_attachment_image_srcset($zoo_img, "product-preview-large");
            ?>
            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"
               style="height:<?php echo esc_attr($zoo_height) ?>px;width:<?php echo esc_attr($zoo_width) ?>px"
               data-resolution="<?php echo esc_attr($resolution) ?>"
               class="wrap-img<?php if (zoo_aternative_images()) echo esc_attr(' has-2imgs'); ?>">
                <img src="<?php echo get_template_directory_uri() . '/assets/images/placeholder.png'; ?>"
                     height="<?php echo esc_attr($zoo_height) ?>" width="<?php echo esc_attr($zoo_width) ?>"
                     class="lazy-img wp-post-image" data-original="<?php echo esc_attr($zoo_img_url) ?>"
                     alt="<?php echo esc_attr($zoo_img_title); ?>"
					 data-srcset="<?php echo esc_attr($zoo_img_srcset) ?>"/>
                <?php
                echo zoo_aternative_images();
                ?>
            </a>
            <?php
        endif;
    }
}
add_action( 'wp_enqueue_scripts', 'fona_child_js_css' );
function fona_child_js_css(){
	
	//wp_dequeue_script( 'woo-variation-swatches' );
	//wp_enqueue_script('homepage-js',get_stylesheet_directory_uri() . '/assets/js/homepage_js.js',array( 'jquery' ));
	wp_register_script('homepage-js',get_stylesheet_directory_uri() . '/assets/js/homepage_js.js',array( 'jquery' ));
	wp_localize_script( 'homepage-js', 'wordpress_general', array( 'ajax_url' => admin_url('admin-ajax.php') ) );
	wp_enqueue_script( 'homepage-js' );
	if(is_product())  {
		if( has_term( 'mangalsutra', 'product_cat' ) || has_term( 'diamond-mangalsutras', 'product_cat' ) || has_term( 'designer-mangalsutra', 'product_cat' ) || has_term( 'traditional-mangalsutra', 'product_cat' )) {
			
			wp_enqueue_script( 'woo-variation-swatches-child', get_stylesheet_directory_uri() . '/assets/js/variation-swatches.js', array( 'jquery' ), false, true );
		}
	}
	wp_enqueue_script('loadmore',get_stylesheet_directory_uri() . '/assets/js/review-loadmore.js', array('jquery'));
	wp_localize_script( 'loadmore', 'review_post_loadmore', array( 'loadmore_url' => admin_url('admin-ajax.php'),'wp_create_load_more' => wp_create_nonce("load_more_posts") ) );
    wp_enqueue_script( 'loadmore' );
	
}


/* about page custo widget */

function arphabet_widgets_init() {

	register_sidebar( array(
		'name'          => 'about page email subscribe',
		'id'            => 'home_right_1',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="rounded">',
		'after_title'   => '</h2>',
	) );

}
add_action( 'widgets_init', 'arphabet_widgets_init' );


function cptui_register_my_cpts() {

	/**
	 * Post Type: reviews.
	 */

	$labels = array(
		"name" => __( "reviews", "" ),
		"singular_name" => __( "review", "" ),
	);

	$args = array(
		"label" => __( "reviews", "" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "review", "with_front" => true ),
		"query_var" => true,
		"supports" => array( "title", "editor", "thumbnail" ),
		"taxonomies" => array( "category" ),
	);

	register_post_type( "review", $args );
}

//add_action( 'init', 'cptui_register_my_cpts' );


add_action( 'woocommerce_product_query', 'all_products_query' );
function all_products_query( $q ){
    if(!is_shop()) {
		$q->set( 'posts_per_page', -1 );
	}
}
include 'compliment-kit.php';

add_action('admin_enqueue_scripts', 'uploader_javaScript_file');
 
 
function uploader_javaScript_file() {
    if (isset($_GET['page']) && $_GET['page'] == 'product_single_page_settings_page') {
        wp_enqueue_media();
        wp_register_script('file_upload_options', get_stylesheet_directory_uri() . '/assets/js/admin-file-upload.js', array('jquery'));
        wp_enqueue_script('file_upload_options');
    }
	if (isset($_GET['page']) && $_GET['page'] == 'postcade-submenu-page') {
        wp_enqueue_media();
        wp_register_script('file_upload_options', get_stylesheet_directory_uri() . '/assets/js/admin-podcast-upload.js', array('jquery'));
        wp_enqueue_script('file_upload_options');
    }
		wp_enqueue_style( 'admin_css', get_stylesheet_directory_uri() . '/assets/css/admin-style.css', false, '1.0.0' );

		wp_enqueue_script('file_meta',get_stylesheet_directory_uri() . '/assets/js/admin-meta.js', array('jquery'));
		wp_enqueue_style( 'wp-color-picker' ); 
		wp_enqueue_script( 'color-picker-inst' ,get_stylesheet_directory_uri() . '/assets/js/color-picker-inst.js', array( 'wp-color-picker' ), false, true ); 
		
		wp_enqueue_media();
		wp_enqueue_script('product_meta_box_enqueue',get_stylesheet_directory_uri() . '/assets/js/product_meta_box_enqueue_script.js', array('jquery'));
}

function woocommerce_default_product_tabs( $tabs = array() ) {
		global $product, $post;

		// Description tab - shows product content.
			$tabs['description'] = array(
				'title'    => __( 'Product Details', 'woocommerce' ),
				'priority' => 10,
				'callback' => 'woocommerce_product_description_tab',
			);
		

		// Additional information tab - shows attributes.
			$tabs['additional_information'] = array(
				'title'    => __( 'Shipping Details', 'woocommerce' ),
				'priority' => 20,
				'callback' => 'woocommerce_product_additional_information_tab',
			);
		

		// Reviews tab - shows comments.
		if ( comments_open() ) {
			$tabs['reviews'] = array(
				/* translators: %s: reviews count */
				'title'    => sprintf( __( 'Reviews (%d)', 'woocommerce' ), $product->get_review_count() ),
				'priority' => 30,
				'callback' => 'comments_template',
			);
		}

		return $tabs;
}

add_filter( 'woocommerce_product_tabs', 'wp_woo_rename_reviews_tab', 98);
function wp_woo_rename_reviews_tab($tabs) {
	if ( comments_open() ) {
		global $product;
		$check_product_review_count = $product->get_review_count();
		if ( $check_product_review_count == 0 ) {
			$tabs['reviews']['title'] = 'Reviews';
		} else {
			$tabs['reviews']['title'] = 'Reviews('.$check_product_review_count.')';
		}
	}
    return $tabs;
}
add_shortcode('date-shipping','date_shipping');
function date_shipping(){
	ob_start();
	?>
	<div class="date_of_shipping_product_single">
	<?php
		echo date('d-m-Y', strtotime("+21 days"))//would normally get printed to the screen/output to browser
	?>
	</div>
	<?php
 $output = ob_get_contents();
ob_end_clean();

return $output;
}
add_shortcode('icon-india','icon_india');
function icon_india(){
	ob_start();
	echo ('/wp-content/uploads/2015/01/download.png');
	$output = ob_get_contents();
	ob_end_clean();
	return $output;
}
add_shortcode('icon-heart','icon_heart');
function icon_heart(){
	ob_start();
	echo ('/wp-content/uploads/2015/01/heart-2-copy.png');
	$output = ob_get_contents();
	ob_end_clean();
	return $output;
}
add_shortcode('icon-feather','icon_feather');
function icon_feather(){
	ob_start();
	echo ('/wp-content/uploads/2015/01/feather-copy.png');
	$output = ob_get_contents();
	ob_end_clean();
	return $output;
}
add_filter( 'meta_content_product_des', 'wptexturize'        );
add_filter( 'meta_content_product_des', 'convert_smilies'    );
add_filter( 'meta_content_product_des', 'convert_chars'      );
add_filter( 'meta_content_product_des', 'wpautop'            );
add_filter( 'meta_content_product_des', 'shortcode_unautop'  );
add_filter( 'meta_content_product_des', 'prepend_attachment' );
add_filter( 'meta_content_product_des', 'do_shortcode');

add_action("add_meta_boxes","add_product_details_meta_box");

function add_product_details_meta_box(){
	add_meta_box("mangalsutra_information", "Mangalsutra Information","mangalsutra_information_meta_box_markup", "product", "normal", "high", null);
	add_meta_box("gemstone_information", "Gemstone Information","gemstone_information_meta_box_markup", "product", "normal", "high", null);
	add_meta_box("luxuriously_handcraft", "Luxuriously Handcraft","luxuriously_handcraft_meta_box_markup", "product", "normal", "high", null);
}

function mangalsutra_information_meta_box_markup($object){
		wp_nonce_field(basename(__FILE__), "mangalsutra-information-nonce");
		$get_data = get_post_meta($object->ID, "mangalsutra_information", true); 
		if(empty($get_data)){
			$get_data =array(1);
		}
		?>
		<table class="mangalsutra_information">
			<tbody>
				<?php
				$foreach_key = 0;
				foreach($get_data as $data){
				?>
				
				<tr class="product_varient mangalsutra_information close_mangalsutra" data-key = "<?php echo($foreach_key) ?>">
					<td class="lab"><label>Title </label></td>
					<td class="url"><input type="text" name="mangalsutra_information[<?php echo($foreach_key) ?>][product_varient_label]" value="<?php echo($data["product_varient_label"]); ?>"></td>
					<td class="lab"><label>Value </label></td>
					<td class="url"><input type="text" name="mangalsutra_information[<?php echo($foreach_key) ?>][product_varient_value]" value="<?php echo($data["product_varient_value"]); ?>"></td>
					<td class="lab"><label>Pop up </label></td>
					<td class="url"><textarea  name="mangalsutra_information[<?php echo($foreach_key) ?>][product_varient_popup]"><?php echo($data["product_varient_popup"]); ?></textarea></td>
					
					<td class="close_the_row"><i class="fa fa-times"></i></td>
				</tr>
				<?php
					$foreach_key++;
				}
				?>
			</tbody>
		</table>
		<div class = "add_btn_container clearfix"><button class="button add_extra_field mangalsutra_information" id="sub">Add Field </button></div>
	<?php 

}

function gemstone_information_meta_box_markup($object){
		wp_nonce_field(basename(__FILE__), "gemstone-information-nonce");
		$get_data = get_post_meta($object->ID, "gemstone_information", true); 
		if(empty($get_data)){
			$get_data =array(1);
		}
		?>
		<table class="gemstone_information">
			<tbody>
				<?php
				$foreach_key = 0;
				foreach($get_data as $data){
				?>
				
				<tr class="product_varient gemstone_information close_gemstone" data-key = "<?php echo($foreach_key) ?>">
					<td class="lab"><label>Title </label></td>
					<td class="url"><input type="text" name="gemstone_information[<?php echo($foreach_key) ?>][product_varient_label]" value="<?php echo($data["product_varient_label"]); ?>"></td>
					<td class="lab"><label>Value </label></td>
					<td class="url"><input type="text" name="gemstone_information[<?php echo($foreach_key) ?>][product_varient_value]" value="<?php echo($data["product_varient_value"]); ?>"></td>
					<td class="lab"><label>Pop up </label></td>
					<td class="url"><textarea  name="gemstone_information[<?php echo($foreach_key) ?>][product_varient_popup]"><?php echo($data["product_varient_popup"]); ?></textarea></td>
					
					<td class="close_the_row"><i class="fa fa-times"></i></td>
				</tr>
				<?php
					$foreach_key++;
				}
				?>
			</tbody>
		</table>
		<div class = "add_btn_container clearfix"><button class="button add_extra_field gemstone_information"  id="sub">Add Field </button></div>
	<?php 

}

function luxuriously_handcraft_meta_box_markup($object){
		wp_nonce_field(basename(__FILE__), "luxuriously-handcraft-nonce");
		$get_data = get_post_meta($object->ID, "luxuriously_handcraft", true); 
		if(empty($get_data)){
			$get_data =array(1);
		}
		?>
		<table class="luxuriously_handcraft">
			<tbody>
				<?php
				$foreach_key = 0;
				foreach($get_data as $data){
				?>
				
				<tr class="product_varient luxuriously_handcraft close_luxuriously_handcraft" data-key = "<?php echo($foreach_key) ?>">
					<td class="lab"><label>Icon </label></td>
					<td class="url"><input type="text" name="luxuriously_handcraft[<?php echo($foreach_key) ?>][product_varient_label]" value="<?php echo($data["product_varient_label"]); ?>"></td>
					<td class="lab"><label>Value </label></td>
					<td class="url"><input type="text" name="luxuriously_handcraft[<?php echo($foreach_key) ?>][product_varient_value]" value="<?php echo($data["product_varient_value"]); ?>"></td>
					<td class="lab"><label>Pop up </label></td>
					<td class="url"><textarea  name="luxuriously_handcraft[<?php echo($foreach_key) ?>][product_varient_popup]"><?php echo($data["product_varient_popup"]); ?></textarea></td>
					
					<td class="close_the_row"><i class="fa fa-times"></i></td>
				</tr>
				<?php
					$foreach_key++;
				}
				?>
			</tbody>
		</table>
		<div class = "add_btn_container clearfix"><button class="button add_extra_field luxuriously_handcraft"  id="sub">Add Field </button></div>
	<?php 

}
add_action("save_post","save_product_details_meta_box", 10, 3);

function save_product_details_meta_box($post_id, $post, $update){
			
		if (!isset($_POST["mangalsutra-information-nonce"]) || !wp_verify_nonce($_POST["mangalsutra-information-nonce"], basename(__FILE__)) || 
		!isset($_POST["gemstone-information-nonce"]) || !wp_verify_nonce($_POST["gemstone-information-nonce"], basename(__FILE__)) ||
		!isset($_POST["luxuriously-handcraft-nonce"]) || !wp_verify_nonce($_POST["luxuriously-handcraft-nonce"], basename(__FILE__))	)
			return $post_id;
		if(!current_user_can("edit_post", $post_id))
			return $post_id;

		if(defined("DOING_AUTOSAVE") && DOING_AUTOSAVE)
			return $post_id;

		$slug = "product";
		if($slug != $post->post_type)
			return $post_id;

		$meta_box_text_value = "";
		
		if(isset($_POST["mangalsutra_information"]))
		{
			$meta_box_text_value = $_POST["mangalsutra_information"];
		}   
		update_post_meta($post_id, "mangalsutra_information", $meta_box_text_value);
		
		if(isset($_POST["gemstone_information"]))
		{
			$meta_box_text_value = $_POST["gemstone_information"];
		}   
		update_post_meta($post_id, "gemstone_information", $meta_box_text_value);
		
		if(isset($_POST["luxuriously_handcraft"]))
		{
			$meta_box_text_value = $_POST["luxuriously_handcraft"];
		}   
		update_post_meta($post_id, "luxuriously_handcraft", $meta_box_text_value);

}


add_shortcode("anatomy-gia", "anatomy_gia");
function anatomy_gia() {
	if ( is_product() ) {
		?>
		<script type="text/javascript" src="https://4cs.gia.edu/interactive-4cs/js/embed.js?tool=anatomy" charset="UTF-8"></script>
		<?php
	}
}


add_shortcode("color-gia", "color_gia");
function color_gia() {
	if ( is_product() ){
		?>
		<script type="text/javascript" src="https://4cs.gia.edu/interactive-4cs/js/embed.js?tool=color" charset="UTF-8"></script>
		<?php
	}
}


add_shortcode("clarity-gia", "clarity_gia");
function clarity_gia() {
	if ( is_product() ){
	?>
	<script type="text/javascript" src="https://4cs.gia.edu/interactive-4cs/js/embed.js?tool=clarity" charset="UTF-8"></script>
	<?php
	}
}


add_shortcode("cut-gia", "cut_gia");
function cut_gia() {
	if ( is_product() ){
	?>
	<script type="text/javascript" src="https://4cs.gia.edu/interactive-4cs/js/embed.js?tool=cut" charset="UTF-8"></script>
	<?php
	}
}


add_shortcode("carat-weight-gia", "carat_weight_gia");
function carat_weight_gia() {
	if ( is_product() ){
	?>
	<script type="text/javascript" src="https://4cs.gia.edu/interactive-4cs/js/embed.js?tool=carat-weight" charset="UTF-8"></script>
	<?php
	}
}

if(isset($_GET['devs'])){

		$luxuriously_handcraft[0]['product_varient_label'] ='[icon-feather]';
		$luxuriously_handcraft[0]['product_varient_value'] ='Designer details in every piece';
		$luxuriously_handcraft[0]['product_varient_popup'] ='';
		$luxuriously_handcraft[1]['product_varient_label'] = '[icon-india]';
		$luxuriously_handcraft[1]['product_varient_value'] ='Handmade in Mumbai, India';
		$luxuriously_handcraft[1]['product_varient_popup'] ='';
		$luxuriously_handcraft[2]['product_varient_label'] ='[icon-heart]';
		$luxuriously_handcraft[2]['product_varient_value'] ='Supports artisans and their families';
		$luxuriously_handcraft[2]['product_varient_popup'] ='';
		
		$query = new WP_Query( array( 'post_type' => 'product','posts_per_page' => -1  ) );
		if ( $query->have_posts() ) {
			while ( $query->have_posts() ) {
				$query->the_post();
				update_post_meta(get_the_ID(), "luxuriously_handcraft", $luxuriously_handcraft);
			}
	
			wp_reset_postdata();
		
		} 
		//update_post_meta(get_the_ID(), "luxuriously_handcraft", $luxuriously_handcraft);

	//$meta_box_text_value["product_varient_label"]=['[icon-feather]','[icon-india]','[icon-heart]'];
	//$meta_box_text_value["product_varient_value"]=['Designer details in every piece','Handmade in Mumbai, India','Supports artisans and their families'];
	die();
		

}

if(isset($_GET['vikas'])){
		echo('<pre>');
		print_r(get_post_meta(4965, "luxuriously_handcraft",'true'));;
		die();
	//$meta_box_text_value["product_varient_label"]=['[icon-feather]','[icon-india]','[icon-heart]'];
	//$meta_box_text_value["product_varient_value"]=['Designer details in every piece','Handmade in Mumbai, India','Supports artisans and their families'];

		

}

//add_action( 'woocommerce_cart_calculate_fees','custom_tax_surcharge_for_swiss', 10, 1 );
function custom_tax_surcharge_for_swiss( $cart ) {
    if ( is_admin() && ! defined('DOING_AJAX') ) return;

    // Only for Swiss country (if not we exit)
    //if ( 'CH' != WC()->customer->get_shipping_country() ) return;

    //$percent = 8;
    $taxes = array_sum( $cart->taxes ); // <=== This is not used in your function

    // Calculation
    //$surcharge = ( $cart->cart_contents_total + $cart->shipping_total ) * $percent / 100;

    // Add the fee (tax third argument disabled: false)
    //$cart->add_fee( __( 'Sales Tax', 'woocommerce'), $taxes, false );
}


add_filter( 'woocommerce_get_order_item_totals', 'dks_tax_new_line', 10, 2 );

function dks_tax_new_line( $total_rows, $myorder_obj ) 
{
	$total_rows['order_total']['value'] = $myorder_obj->get_formatted_order_total();

	//Make an array with tax
	$taxes = array();
	foreach ( $myorder_obj->get_tax_totals() as $code => $tax ) {
	$taxes[] = sprintf( '%s', $tax->formatted_amount );
	}

	//Add the tax row to the array that get_order_item_totals() outputs
	$total_rows['tax_line'] = array(
	'label' => 'Sales Tax:',
	'value'	=> sprintf( implode( ', ', $taxes ) )
	);

	return $total_rows;
}

function plugin_function_custom_overide( $number ) {
    // Maybe modify $example in some way.
    return 10;
}
add_filter( 'alg_wc_currency_switcher_plugin_option', 'plugin_function_custom_overide' );
function createSlug_smp($str, $delimiter = '-'){

    $slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));
    return $slug;

} 

if ( ! function_exists( 'woocommerce_variable_add_to_cart' ) ) {
function woocommerce_variable_add_to_cart() {
		global $product;

		// Enqueue variation scripts.
		wp_enqueue_script( 'wc-add-to-cart-variation' );

		// Get Available variations?
		$get_variations = count( $product->get_children() ) <= apply_filters( 'woocommerce_ajax_variation_threshold', 30, $product );

		// Load the template.
		if( has_term( 'mangalsutra', 'product_cat' ) || has_term( 'diamond-mangalsutras', 'product_cat' ) || has_term( 'designer-mangalsutra', 'product_cat' ) || has_term( 'traditional-mangalsutra', 'product_cat' )) {
			wc_get_template( 'single-product/add-to-cart/variable_mangalsutra.php', array(
				'available_variations' => $get_variations ? $product->get_available_variations() : false,
				'attributes'           => $product->get_variation_attributes(),
				'selected_attributes'  => $product->get_default_attributes(),
			) );
		}else{
			wc_get_template( 'single-product/add-to-cart/variable.php', array(
				'available_variations' => $get_variations ? $product->get_available_variations() : false,
				'attributes'           => $product->get_variation_attributes(),
				'selected_attributes'  => $product->get_default_attributes(),
			) );
		}
}
}
add_filter('woocommerce_variable_price_html', 'custom_variation_price', 10, 2); 

function custom_variation_price( $price, $product ) { 
	if( has_term( 'mangalsutra', 'product_cat' ) || has_term( 'diamond-mangalsutras', 'product_cat' ) || has_term( 'designer-mangalsutra', 'product_cat' ) || has_term( 'traditional-mangalsutra', 'product_cat' )) {
		
		 $price = '';

		 $price .= wc_price($product->get_price()); 
	}
     return $price;
}
if ( ! function_exists( 'woocommerce_template_single_price' ) ) {
	function woocommerce_template_single_price() {
		global $product;
		$id = $product->id;
		if( has_term( 'mangalsutra', 'product_cat' ) || has_term( 'diamond-mangalsutras', 'product_cat' ) || has_term( 'designer-mangalsutra', 'product_cat' ) || has_term( 'traditional-mangalsutra', 'product_cat' )) {
			?>
			<div class="variation_mangalsutra_single">
		    <span class="woocommerce-variation-price">
				<span class="price">
				<?php
				   if ($product->is_type( 'simple' )) { 
							echo $product->get_price_html(); 
					}
                elseif($product->product_type=='variable') {
                    $available_variations = $product->get_available_variations();
					$total_prices = array();
						foreach($available_variations as $available_variation){
							$variation_id = $available_variation['variation_id'];
							$variable_product1= new WC_Product_Variation( $variation_id );
							$regular_price = $variable_product1 ->regular_price;
							$sales_price = $variable_product1 ->sale_price;
							if($sales_price == ''){
								$sales_price = 0;
							}
							$total_prices[] = $regular_price+$sales_price;
						}
						sort($total_prices);
						echo(woocommerce_price($total_prices[0]));
                    }
					?>
				</span>
			</span>
			</div>
			 <span class="change_currency_product_single">
				<?php
					echo do_shortcode('[woocommerce_currency_switcher_drop_down_box]');
				?>
			 </span>
		<?php
		}else{
			wc_get_template( 'single-product/price.php' );
		}
	}
	
}
function create_podcast_post_type() {
  register_post_type( 'podcast',
    array(
      'labels' => array(
        'name' => __( 'Podcast' ),
        'singular_name' => __( 'Podcast' )
      ),
      'public' => true,
      'has_archive' => true,
      'supports' => array( 'title', 'editor','comments', 'author','thumbnail' ),
	   'menu_icon'           => 'dashicons-microphone',

    )
  );
  add_action( 'init', 'create_book_tax' );

	register_taxonomy(
		'podcast_cat',
		'podcast',
		array(
			'label' => __( 'Podcast Category' ),
			'rewrite' => array( 'slug' => 'podcast_category' ),
			'hierarchical' => true,
			'query_var' => true,
		)
	);

}

add_action( 'init', 'create_podcast_post_type' );
// include Podcast Setinng page
include 'podcast-settings.php';

add_shortcode( 'social-subscribe-podcast', 'social_subscribe_podcast' );

function social_subscribe_podcast(){
	ob_start();
	$options = get_option( 'podcast_settings', array() );
	?>
	<div class="social_subscribe_podcast_container">
		<?php
		foreach($options as $data){
		?>	
		<div class="social_subscribe_podcast_sub_container">
			<div class="social_subscribe_podcast_sub_left_container">
			<?php 
			if($data['label_anchor']== ""){
				?>
				<img src="<?php echo($data['product_upload_image']); ?>">
			<?php 
			}else{
				?>
				<a href="<?php echo($data['label_anchor']); ?>"><img src="<?php echo($data['product_upload_image']); ?>"></a>
				<?php
			}
			?>
			</div>
			<div class="social_subscribe_podcast_sub_right_container">
			<?php 
			if($data['label_anchor']== ""){
				?>
				<p class="social_subscribe_podcast_label"> <?php echo($data['label_to_logo']); ?></p>
			<?php 
			}else{
				?>
				<a href="<?php echo($data['label_anchor']); ?>"><p class="social_subscribe_podcast_label"> <?php echo($data['label_to_logo']); ?></p></a>
				<?php
			}
			?>
			</div>
		</div>
		<?php
		}
	?>
	</div>
<?php
   $output = ob_get_contents();
   ob_end_clean();
   return $output;
}
add_shortcode( 'podcast-category', 'podcast_category_filter' );

function podcast_category_filter(){
	
	ob_start();
	?>
	<div id="container_podcast_archive_filter">
		<?php
		if( $terms = get_terms( array('taxonomy' => 'podcast_cat','hide_empty' => false) )) : // to make it simple I use default categories
			echo '<div class="select-container-podcast"><div class="header_select_podcast"> What are you looking for?</div><select id="podcast_filter" name="categoryfilter"><option disabled selected value="default">Category</option>';
			foreach ( $terms as $term ) :
				echo '<option value="' . $term->term_id  . '">' . $term->name . '</option>'; // ID of the category as the value of an option
			endforeach;
			echo '</select></div>';
		endif;
		?>
		<div class="podcast_cover_container">
			<div id="response" class="podcast_sub_cover clearfix"></div>
		</div>
	</div>
	<?php
   $output = ob_get_contents();
   ob_end_clean();
   return $output;
	
}

function podcast_filter_function(){

	if( isset( $_POST['categoryfilter'] ) && $_POST['categoryfilter'] == null ){
			$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
				$args = array(
				 'post_type'        => 'podcast',
				 'post_status'     => 'publish',
				 'posts_per_page'   => 6,
				  'paged'	=> $paged,
				  
				);
		}else{
			if( isset( $_POST['categoryfilter'] ) ){
				$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
			
				$args = array(
					'post_type' => 'podcast',
					'post_status'     => 'publish',
					'posts_per_page' => 6,
					 'paged'	=> $paged,
					'tax_query' => array(
						array(
							'taxonomy' => 'podcast_cat',
							'field' => 'id',
							'terms' => $_POST['categoryfilter'],
						),
					),
				);
			}
		}
		$query = new WP_Query( $args ); 
		if(isset($_POST['categoryfilter'])){
			$category_filter = $_POST['categoryfilter'];
		}else{
			$category_filter = "";
		}
		?>
		<div class="category_filter_container_module clearfix" >
		<?php
		if ( $query->have_posts() ) {
		while ( $query->have_posts() ) {
			$query->the_post(); 
			$link = get_post_permalink( get_the_ID() );
			$total_pages = $query->max_num_pages;
			
			?>
			<div class="category_filter_container">
				<?php if (has_post_thumbnail( get_the_ID()) ): ?>
					  <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID()), 'single-post-thumbnail' ); ?>
						<a href="<?php echo($link); ?>" target="_blank">
							<div id="podcast-image-bg" style="background-image: url('<?php echo $image[0]; ?>')"></div>
						</a>
				<?php endif; ?>
			<a href="<?php echo($link); ?>" target="_blank"><h4 class="podcast_header"><?php echo($query->post->post_title); ?></h4></a>
			</div>
			<?php
			} 
			?>
			</div>
			<?php 
			if($total_pages != $paged){
			?>
			<div class="load_more">
				<button  type="button" class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-round vc_btn3-style-custom" id="load_more_posts" data-total_pages="<?php echo($total_pages); ?>"data-paged ="<?php echo($paged);?>" data-category = "<?php echo($category_filter);?>"> Load more..</button>
			</div>
			<?php
			}
		}else{
			echo 'No posts found';
		}
		wp_reset_query();
	
	
	die();
}
 
 
add_action('wp_ajax_podcast_filter_function', 'podcast_filter_function'); 
add_action('wp_ajax_nopriv_podcast_filter_function', 'podcast_filter_function');

 add_action( 'wp_ajax_load_more_posts', 'load_more_posts' );
add_action( 'wp_ajax_nopriv_load_more_posts', 'load_more_posts' );

function load_more_posts(){

	if(isset($_POST['category']) && $_POST['category'] !== ''){
		$args = array(
					'post_type' => 'podcast',
					'post_status' => 'publish',
					'posts_per_page' => 12,
					'offset' => (6 + (($_POST['paged'] -1) * 12)),
					'tax_query' => array(
						array(
							'taxonomy' => 'podcast_cat',
							'field' => 'id',
							'terms' => $_POST['category'],
						),
					),
				);
	}
	else{
			$args = array(
					'post_type'=>'podcast',
					'post_status' => 'publish', 
					'offset' => (6 + (($_POST['paged'] -1) * 12)) , 
					'posts_per_page'=> 12
					);
	}
	$query = new WP_Query($args);
	if($query->have_posts()):
		while($query->have_posts()):$query->the_post();
			$link = get_post_permalink( get_the_ID() );
		
				?>
				<div class="category_filter_container">
					<?php if (has_post_thumbnail( get_the_ID()) ): ?>
						  <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID()), 'single-post-thumbnail' ); ?>
							<a href="<?php echo($link); ?>" target="_blank">
								<div id="podcast-image-bg" style="background-image: url('<?php echo $image[0]; ?>')"></div>
							</a>
					<?php endif; ?>
				<a href="<?php echo($link); ?>" target="_blank"><h4 class="podcast_header"><?php echo($query->post->post_title); ?></h4></a>
			</div>
			<?php
		endwhile;
		wp_reset_postdata();
	endif;
	die();
}

function podcast_sidebar_widgets_init() {

	register_sidebar( array(
		'name'          => 'Podcast sidebar',
		'id'            => 'podcast_sidebar',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="rounded">',
		'after_title'   => '</h2>',
	) );

}
add_action( 'widgets_init', 'podcast_sidebar_widgets_init' );
// include custom widgets
include 'podcast_category_widget.php';

function podcast_category_widget() {
    register_widget( 'WP_Custom_Post_Type_Widgets_Categories' );
}
add_action( 'widgets_init', 'podcast_category_widget' );
if(isset($_GET['test-543'])){
	global $product;
	echo('<pre>');
	print_r(get_post_meta(3654));
	echo('</pre>');
	die();
}
add_action('add_meta_boxes', 'add_upload_file_metaboxes');

function add_upload_file_metaboxes() {
    add_meta_box('swp_file_upload', 'Author Image', 'swp_podcast_file_upload', 'podcast', 'side', 'default');
}


function swp_podcast_file_upload() {
    global $post;
    echo '<input type="hidden" name="podcastmeta_noncename" id="podcastmeta_noncename" value="'.
    wp_create_nonce(plugin_basename(__FILE__)).
    '" />';
    global $wpdb;
    $strFile = get_post_meta($post -> ID, $key = 'podcast_file', true);
    $media_file = get_post_meta($post -> ID, $key = '_wp_attached_file', true);
    if (!empty($media_file)) {
        $strFile = $media_file;
    } ?>


    <script type = "text/javascript">

	var file_frame;
	jQuery('#upload_image_button_podcast').live('click', function(e) {
		
        e.preventDefault();

        if (file_frame) {
            file_frame.open();
            return;
        }

        file_frame = wp.media.frames.file_frame = wp.media({
            title: jQuery(this).data('uploader_title'),
            button: {
                text: jQuery(this).data('uploader_button_text'),
            },
            multiple: false 
        });

        file_frame.on('select', function(){
            attachment = file_frame.state().get('selection').first().toJSON();

            var url = attachment.url;
            var field = document.getElementById("podcast_file");
			
            field.value = url; 
			$(".podcast_author_image").attr('src', url );
			
			$(".podcast_author_image").css('display', "block" );
			jQuery("#delete_image_button_podcast").css('display', "block" );
        });
		
        file_frame.open();
    });
	jQuery('#delete_image_button_podcast').live('click', function(e) {
		jQuery(".podcast_author_image").attr('src', "" );
		jQuery(".podcast_author_image").css('display', "none" );
		jQuery("#delete_image_button_podcast").css('display', "none" );
		jQuery("#podcast_file").val( "" );
	});
    </script>
	<div>
		<table>
		<tr valign = "top">
			<td>
				<div>
					<input type= "hidden" name = "podcast_file" id = "podcast_file" value = "<?php echo $strFile; ?>" />
					<img class="podcast_author_image <?php if (!empty($strFile)) { echo("url_present"); }?>" src="<?php echo $strFile; ?>" style="width:250px;height:250px">
				</div>
				<div class="wp-core-ui" style="padding-top:12px">
					<input id = "upload_image_button_podcast" class=" button button-primary button-large" type="button" value="Upload" style="float: left;" >
					<input id = "delete_image_button_podcast" class="<?php if (!empty($strFile)) { echo("url_present"); }?>  button button-primary button-large" type = "button" value = "Delete" style="float: right;">
				</div>
			</td> 
		</tr>
		</table> 
	</div> 
	<style>
	.podcast_author_image{
		display:none;
	}
	.podcast_author_image.url_present{
		display:block;
	}
	#delete_image_button_podcast{
		display:none;
	}
	#delete_image_button_podcast.url_present{
		display:block;
	}
	.wp-core-ui #delete_image_button_podcast.button-primary:active{
		background: #cc3c31;
		border-color: #cc3c31;
		box-shadow: inset 0 2px 0 #cc3c31;
	}
	.wp-core-ui .button-primary.focus, .wp-core-ui #delete_image_button_podcast.button-primary:focus {
		box-shadow: 0 1px 0 #cc3c31, 0 0 2px 1px #cc3c31;
	}
	.wp-core-ui #delete_image_button_podcast.button:focus {
		border-color: #5b9dd9;
		box-shadow: 0 0 3px rgba(0,115,170,.8);
	}
	.wp-core-ui #delete_image_button_podcast.button-primary {
		background: #F44336;
		border-color: #cc3c31 #cc3c31 #cc3c31;
		box-shadow: 0 1px 0 #cc3c31;
		color: #fff;
		text-decoration: none;
		text-shadow: 0 -1px 1px #cc3c31, 1px 0 1px #cc3c31, 0 1px 1px #cc3c31, -1px 0 1px #cc3c31;
	}
	</style>
	<?php

}


function save_podcasts_meta($post_id, $post) {
    if (!wp_verify_nonce($_POST['podcastmeta_noncename'], plugin_basename(__FILE__))) {
        return $post -> ID;
    }
    if (!current_user_can('edit_post', $post -> ID))
        return $post -> ID;
    $podcasts_meta['podcast_file'] = $_POST['podcast_file'];

    foreach($podcasts_meta as $key => $value) {
        if ($post -> post_type !='podcast') return;
        $value = implode(',', (array) $value);
        if (get_post_meta($post -> ID, $key, FALSE)) { 
            update_post_meta($post -> ID, $key, $value);
        } else { 
            update_post_meta($post -> ID, $key, $value);
        }
        if ($value == "") delete_post_meta($post -> ID, $key); 
    }
}
add_action('save_post', 'save_podcasts_meta', 1, 2); 

function gravatar_upload_endpoints() {
    add_rewrite_endpoint( 'gravatar-upload', EP_ROOT | EP_PAGES );
}

add_action( 'init', 'gravatar_upload_endpoints' );

function gravatar_upload_query_vars( $vars ) {
    $vars[] = 'gravatar-upload';

    return $vars;
}

add_filter( 'query_vars', 'gravatar_upload_query_vars', 0 );

function gravatar_upload_flush_rewrite_rules() {
    flush_rewrite_rules();
}

add_action( 'wp_loaded', 'gravatar_upload_flush_rewrite_rules' );

function gravatar_upload_my_account_menu_items( $items ) {
    $items = array(
        'dashboard'         => __( 'Dashboard', 'woocommerce' ),
        'orders'            => __( 'Orders', 'woocommerce' ),
        'downloads'       => __( 'Downloads', 'woocommerce' ),
        'edit-address'    => __( 'Addresses', 'woocommerce' ),
        'payment-methods' => __( 'Payment Methods', 'woocommerce' ),
        'edit-account'      => __( 'Edit Account', 'woocommerce' ),
        'gravatar-upload'   => __('Avatar', 'woocommerce' ),
        'customer-logout'   => __( 'Logout', 'woocommerce' ),
    );

    return $items;
}

add_filter( 'woocommerce_account_menu_items', 'gravatar_upload_my_account_menu_items' );

function gravatar_upload_endpoint_content() {
   
		echo do_shortcode('[avatar_upload]');	
}

add_action( 'woocommerce_account_gravatar-upload_endpoint', 'gravatar_upload_endpoint_content',200 );

/* woocommerce custom meta */
add_action('product_cat_add_form_fields', 'wh_taxonomy_add_new_meta_field');
add_action('product_cat_edit_form_fields', 'wh_taxonomy_edit_meta_field');

//Product Cat Create page
function wh_taxonomy_add_new_meta_field() {
	?>
	<div class="form-field">
		<label for="image_position_cat"><?php echo('Image Position'); ?></label>
		<select name="image_position_cat" id="image_position_cat">
		  <option value="right">Right</option>
		  <option value="left">Left</option>
		  <option value="cover">Cover</option>
		</select>
		<p class="description"><?php echo('Select image Position'); ?></p>
	</div>
	<div class="form-field">
		<label for="image_side_background"><?php echo('Header Background Color'); ?></label>
		<input type="text" name="image_side_background" id="image_side_background">
		<p class="description"><?php echo('Enter background color for header'); ?></p>
	</div>
	<div class="form-field">
		<label for="header_title_color"><?php echo('Header Title Color'); ?></label>
		<input type="text" name="header_title_color" id="header_title_color">
		<p class="description"><?php echo('Enter color for title'); ?></p>
	</div>

	<?php
}

//Product Cat Edit page
function wh_taxonomy_edit_meta_field($term) {
	//getting term ID
	$termId = $term->term_id;
	// retrieve the existing value(s) for this meta field.
	$imageSideBackground = get_term_meta($termId, 'image_side_background', true);
	$imagePositionCat = get_term_meta($termId, 'image_position_cat', true);
	$headerTitleColor = get_term_meta($termId, 'header_title_color', true);
	?>
	<tr class="form-field">
		<th scope="row" valign="top"><label for="image_position_cat">Image Position</label></th>
		<td>	
			<select name="image_position_cat" id="image_position_cat">
			  <option <?php if ($imagePositionCat == 'right') { ?> selected <?php }; ?> value="right">Right</option>
			  <option <?php if ($imagePositionCat == 'left') { ?> selected <?php }; ?> value="left">Left</option>
			  <option <?php if ($imagePositionCat == 'cover') { ?> selected <?php }; ?> value="cover">Cover</option>
			</select>
			<p class="description"><?php echo('Select image Position'); ?></p>
		</td>
	</tr>
	<tr class="form-field">
		<th scope="row" valign="top"><label for="image_side_background">Header Background Color</label></th>
		<td>
			<input type="text" name="image_side_background" id="image_side_background" value="<?php echo esc_attr($imageSideBackground) ? esc_attr($imageSideBackground) : ''; ?>">
			<p class="description">Enter background color for header</p>
		</td>
	</tr>
	<tr class="form-field">
		<th scope="row" valign="top"><label for="header_title_color">Header Title Color</label></th>
		<td>
			<input type="text" name="header_title_color" id="header_title_color" value="<?php echo esc_attr($headerTitleColor) ? esc_attr($headerTitleColor) : ''; ?>">
			<p class="description">Enter color for title</p>
		</td>
	</tr>
	<?php
}

add_action('edited_product_cat', 'wh_save_taxonomy_custom_meta', 10, 1);
add_action('create_product_cat', 'wh_save_taxonomy_custom_meta', 10, 1);

// Save extra taxonomy fields callback function.
function wh_save_taxonomy_custom_meta($termId) {
	$imageSideBackground = filter_input(INPUT_POST, 'image_side_background');
	$imagePositionCat = filter_input(INPUT_POST, 'image_position_cat');
	$headerTitleColor = filter_input(INPUT_POST, 'header_title_color');
	update_term_meta($termId, 'image_side_background', $imageSideBackground);
	update_term_meta($termId, 'image_position_cat', $imagePositionCat);
	update_term_meta($termId, 'header_title_color', $headerTitleColor);
}


add_action('wp_ajax_load_posts_by_ajax', 'load_posts_by_ajax_callback');
add_action('wp_ajax_nopriv_load_posts_by_ajax', 'load_posts_by_ajax_callback');


function load_posts_by_ajax_callback() {
   check_ajax_referer('load_more_posts', 'security');
   $paged = $_POST['page'];
   $args = array(
'post_type' => 'review',
'post_status' => 'publish',
'posts_per_page' => '4',
'paged' => $paged,
);
$my_posts = new WP_Query( $args );
if ( $my_posts->have_posts() ) :
?>
<article>
<?php while ( $my_posts->have_posts() ) : $my_posts->the_post() ?>
<div class="masonry-entry clearfix">
					<?php 
					if ( has_post_thumbnail() ) :
					?>
					<div class="masonry-thumbnail">
						<?php 
						the_post_thumbnail('masonry-thumb'); 
						?>
					</div>
					<?php 
					endif; 
					?>
					<div class="masonry-content">
						<div class="review_post_title"><?php the_title(); ?> </div>
						<div class="review_post_content"><?php the_content();?></div>
					</div>
				</div>
<?php endwhile ?>
</article>
<?php
endif;

wp_die();
}
/*
function reviews_area_widgets() {

	register_sidebar( array(
		'name'          => 'Reviews Area',
		'id'            => 'reviews_area',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="reviews_area_widgets">',
		'after_title'   => '</h2>',
	));

}
add_action( 'widgets_init', 'reviews_area_widgets' );
*/
remove_action( 'wp_enqueue_scripts', 'astoundify_recaptcha_register_script', 1 );
remove_action( 'admin_enqueue_scripts', 'astoundify_recaptcha_register_script', 1 );
remove_action( 'login_enqueue_scripts', 'astoundify_recaptcha_register_script', 1 );

add_action( 'wp_enqueue_scripts', 'astoundify_recaptcha_register_script_edited', 1 );
function astoundify_recaptcha_register_script_edited($page) {
	if ( is_page( 'my-account' ) ) {
		// Only load if site key is set.
		$site_key = astoundify_recaptcha_site_key();
		if ( ! $site_key ) {
		
			return;
		}
		// Google reCAPTCHA scripts. Add prefix to make sure it's loaded if other recaptcha plugin also load it.
		wp_register_script( 'astoundify-google-recaptcha', 'https://www.google.com/recaptcha/api.js?onload=astoundifyRecaptcha&render=explicit', array(), '1.0.0', false );

		// Loader callback.
		wp_register_script( 'astoundify-recaptcha', ASTOUNDIFY_RECAPTCHA_URL . 'resources/assets/js/recaptcha.js', array( 'astoundify-google-recaptcha', 'jquery' ),'1.0.0' );
		wp_localize_script(
			'astoundify-recaptcha', 'astoundifyRecaptchaData', array(
				'sitekey' => esc_attr( $site_key ),
			)
		);

		// Auto load script if enabled.
		
		if ( ! is_user_logged_in() && ! is_admin() && astoundify_recaptcha_get_option( 'enqueue_js', false ) ) {
			wp_enqueue_script( 'astoundify-recaptcha' );
		}
	}
}


	add_action( 'add_meta_boxes', 'product_add_meta_box');
	function product_add_meta_box(){
		add_meta_box(  'screen-product','Product image2','product_meta_box_content','product', 'side');
	}
	 
	function product_meta_box_content(){
		$post_id = get_the_ID();
		$product_single_various_thumbnail_image = get_post_meta( $post_id,'product_single_various_thumbnail_image',true);
		?>
		<p class="hide-if-no-js">
			<a title="Set Footer Image" class="<?php if($product_single_various_thumbnail_image != ''){ echo('hidden'); } ?>" href="#" id="product-various-thumbnail">Set featured image</a>
			<a title="Set Footer Image" href="#" id="product-various-thumbnail-img">
				<img id="product-various-thumbnail-image-preview" src="<?php if(isset($product_single_various_thumbnail_image)){ echo($product_single_various_thumbnail_image); } ?>">
			</a>
			<div>
				<a href="#" class="<?php if($product_single_various_thumbnail_image  == ''){ echo('hidden'); } ?>" id="product-various-thumbnail-remove">Remove product image</a>
			</div>
			<input type="hidden" type="text" id="product-various-thumbnail-image-id" name="product_single_various_thumbnail_image" value="<?php if(isset($product_single_various_thumbnail_image)){ echo($product_single_various_thumbnail_image); } ?>" />
		</p>
		<?php
	}
	add_action('save_post','save_product_single_various_callback');
	function save_product_single_various_callback($post_id){
		global $post; 
		if ($post->post_type == 'product'){
			if(isset($_POST['product_single_various_thumbnail_image'])){
				$post_id = get_the_ID();
				update_post_meta( $post_id,'product_single_various_thumbnail_image', $_POST['product_single_various_thumbnail_image']);
			}
			return;
		}
	}
	
	if (!function_exists('zoo_aternative_images')) {
		function zoo_aternative_images()
		{
			if (get_theme_mod('zoo_aternative_images', '0') != '0') {
				$id = get_the_ID();
				$gallery = get_post_meta($id, '_product_image_gallery', true);
				$product_single_various_thumbnail_image = get_post_meta( $id,'product_single_various_thumbnail_image',true);
				if($product_single_various_thumbnail_image != ''){
					return '<div class="product-single-shop-image-hover" style="background-image:url('.($product_single_various_thumbnail_image) .');"></div>';
				}else{
					if (!empty($gallery)) {
						$gallery = explode(',', $gallery);
						$first_image_id = $gallery[0];
						$zoo_item = wp_get_attachment_image_src($first_image_id,'full');
						$zoo_img_url = $zoo_item[0];
						$zoo_width = $zoo_item[1];
						$zoo_height = $zoo_item[2];
						$zoo_img_title = get_the_title($first_image_id);
						$zoo_img_srcset = wp_get_attachment_image_srcset($first_image_id);
						return '<div class="product-single-shop-image-hover"  srcset="' . esc_attr($zoo_img_srcset) . '" style="background-image:url('.esc_attr($zoo_img_url) .');"></div>';
					}
				}
				return false;
			}
		}
	}

?>
