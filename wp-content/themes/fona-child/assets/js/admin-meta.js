 jQuery(document).ready(function($){  
   $(".add_extra_field.mangalsutra_information").click(function(event){
		event.preventDefault();
	    var row_key = $(".mangalsutra_information tbody tr:last-child").data('key');
		var get_int_key = parseInt(row_key)+1;
		var to_append = '<tr class="product_varient mangalsutra_information close_mangalsutra" data-key = "'+get_int_key+'">'+
							'<td class="lab">'+
								'<label>Title </label>'+
							'</td>'+
							'<td class="url">'+
								'<input type="text" name="mangalsutra_information['+get_int_key+'][product_varient_label]" value="">'+
								'</td>'+
							'<td class="lab">'+
								'<label>Value </label>'+
							'</td>'+
							'<td class="url">'+
								'<input type="text" name="mangalsutra_information['+get_int_key+'][product_varient_value]" value="">'+
							'</td>'+
							'<td class="lab">'+
								'<label>Pop up </label>'+
							'</td>'+
							'<td class="url">'+
								'<textarea  name="mangalsutra_information['+get_int_key+'][product_varient_popup]"></textarea>'+
							'</td>'+
							'<td class="close_the_row">'+
								'<i class="fa fa-times">'+
							     '</i>'+
							'</td>'+
						'</tr>';
		$(".mangalsutra_information tbody").append(to_append);
	});
	$(".mangalsutra_information tbody").on("click",".close_mangalsutra .close_the_row",function(){
		$(this).parents('.close_mangalsutra').remove();
	});
	
	$(".add_extra_field.gemstone_information").click(function(event){
		event.preventDefault();
	    var row_key = $(".gemstone_information tbody tr:last-child").data('key');
		var get_int_key = parseInt(row_key)+1;
		var to_append = '<tr class="product_varient gemstone_information close_gemstone" data-key = "'+get_int_key+'">'+
							'<td class="lab">'+
								'<label>Title </label>'+
							'</td>'+
							'<td class="url">'+
								'<input type="text" name="gemstone_information['+get_int_key+'][product_varient_label]" value="">'+
								'</td>'+
							'<td class="lab">'+
								'<label>Value </label>'+
							'</td>'+
							'<td class="url">'+
								'<input type="text" name="gemstone_information['+get_int_key+'][product_varient_value]" value="">'+
							'</td>'+
							'<td class="lab">'+
								'<label>Pop up </label>'+
							'</td>'+
							'<td class="url">'+
								'<textarea  name="gemstone_information['+get_int_key+'][product_varient_popup]"></textarea>'+
							'</td>'+
							'<td class="close_the_row">'+
								'<i class="fa fa-times">'+
							     '</i>'+
							'</td>'+
						'</tr>';
		$(".gemstone_information tbody").append(to_append);
	});
	
	$(".gemstone_information tbody").on("click",".close_gemstone .close_the_row",function(){
		$(this).parents('.close_gemstone').remove();
	});
	$(".add_extra_field.luxuriously_handcraft").click(function(event){
		event.preventDefault();
	    var row_key = $(".luxuriously_handcraft tbody tr:last-child").data('key');
		var get_int_key = parseInt(row_key)+1;
		var to_append = '<tr class="product_varient luxuriously_handcraft close_luxuriously_handcraft" data-key = "'+get_int_key+'">'+
							'<td class="lab">'+
								'<label>Title </label>'+
							'</td>'+
							'<td class="url">'+
								'<input type="text" name="luxuriously_handcraft['+get_int_key+'][product_varient_label]" value="">'+
								'</td>'+
							'<td class="lab">'+
								'<label>Value </label>'+
							'</td>'+
							'<td class="url">'+
								'<input type="text" name="luxuriously_handcraft['+get_int_key+'][product_varient_value]" value="">'+
							'</td>'+
							'<td class="lab">'+
								'<label>Pop up </label>'+
							'</td>'+
							'<td class="url">'+
								'<textarea  name="luxuriously_handcraft['+get_int_key+'][product_varient_popup]"></textarea>'+
							'</td>'+
							'<td class="close_the_row">'+
								'<i class="fa fa-times">'+
							     '</i>'+
							'</td>'+
						'</tr>';
		$(".luxuriously_handcraft tbody").append(to_append);
	});
	$(".luxuriously_handcraft tbody").on("click",".close_luxuriously_handcraft .close_the_row",function(){
		$(this).parents('.close_luxuriously_handcraft').remove();
	});
	
});