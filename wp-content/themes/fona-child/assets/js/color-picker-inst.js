(function( $ ) {
 
    // Add Color Picker to all inputs that have 'color-field' class
    $(function() {
        $('#image_side_background').wpColorPicker();
	$('#header_title_color').wpColorPicker();
    });
     
})( jQuery );
