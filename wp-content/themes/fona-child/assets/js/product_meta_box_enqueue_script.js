/**
 * Callback function for the 'click' event of the 'Set Footer Image'
 * anchor in its meta box.
 *
 * Displays the media uploader for selecting an image.
 *
 * @since 0.1.0
 */
 jQuery(document).ready(function($){
	  var custom_uploader;
	$('#product-various-thumbnail').click(function(e) {
		e.preventDefault();
		//If the uploader object has already been created, reopen the dialog
		if (custom_uploader) {
			custom_uploader.open();
			return;
		}
		//Extend the wp.media object
		custom_uploader = wp.media.frames.file_frame = wp.media({
			title: 'Choose Image',
			button: {
				text: 'Choose Image'
			},
			multiple: false
		});
		//When a file is selected, grab the URL and set it as the text field's value
		custom_uploader.on('select', function() {
			attachment = custom_uploader.state().get('selection').first().toJSON();
			$('#product-various-thumbnail-image-id').val(attachment.url);
			$("#product-various-thumbnail-image-preview").attr("src", attachment.url );
			$('#product-various-thumbnail').hide();
			$('#product-various-thumbnail-remove').show();
		});
		//Open the uploader dialog
		custom_uploader.open();
	});
	$('body').on('click','#product-various-thumbnail-img', function(e) {
			e.preventDefault();
		//If the uploader object has already been created, reopen the dialog
		if (custom_uploader) {
			custom_uploader.open();
			return;
		}
		//Extend the wp.media object
		custom_uploader = wp.media.frames.file_frame = wp.media({
			title: 'Choose Image',
			button: {
				text: 'Choose Image'
			},
			multiple: false
		});
		//When a file is selected, grab the URL and set it as the text field's value
		custom_uploader.on('select', function() {
			attachment = custom_uploader.state().get('selection').first().toJSON();
			$('#product-various-thumbnail-image-id').val(attachment.url);
			$("#product-various-thumbnail-image-preview").attr("src", attachment.url );
		});
		//Open the uploader dialog
		custom_uploader.open();
		
	});
	$('body').on('click','#product-various-thumbnail-remove', function(e) {
		$('#product-various-thumbnail').show();
		$('#product-various-thumbnail-image-id').val('');
		$("#product-various-thumbnail-image-preview").attr("src", '' );
		$('#product-various-thumbnail-remove').hide();
	});
});
