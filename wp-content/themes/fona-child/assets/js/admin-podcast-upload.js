jQuery(document).ready(function($){
	$('body').on("click",".podcast_image_upload",function(e) {
		var btthis = $(this);
        e.preventDefault();
        //Extend the wp.media object
        custom_uploader = wp.media.frames.file_frame = wp.media({
            title: 'Choose Image',
            button: {
                text: 'Choose Image'
            },
            multiple: false
        });
		
        //When a file is selected, grab the URL and set it as the text field's value
        custom_uploader.on('select', function() {
            attachment = custom_uploader.state().get('selection').first().toJSON();
            btthis.parents(".podcast_settings_container").find('.podcast_upload_url').val(attachment.url);
			btthis.parents(".podcast_settings_container").find('.podcast-image-preview').attr("src", attachment.url );
        });
		
        //If the uploader object has already been created, reopen the dialog
        if (custom_uploader) {
            custom_uploader.open();
            return;
        }
   });
   $(".podcast_reapeter").click(function(event){
	   event.preventDefault();
		var row_key = $(".podcast_container .podcast_settings_container:last-child").data('key');
		var get_int_key = parseInt(row_key)+1;
		var to_append = '<div class="podcast_settings_container close_container" data-key ="'+get_int_key+'">'+
					'<div class="podcast_upload_settings_container">'+
							'<img style="width: 100px;" class="podcast-image-preview" src="">'+
							'<input type="button" value="Upload Image/Select Image" class="podcast_image_upload" id="podcast_upload_image_button" name="podcast_settings['+get_int_key+'][podcast_upload_image_button]">'+
							'<input type="hidden" class="podcast_upload_url" size="36" id="product_upload_image" name="podcast_settings['+get_int_key+'][product_upload_image]">'+
					'</div>'+
					'<div class="podcast_label_settings_container">'+
						'<span class="label_to_logo_container">'+
							'<input type="text" placeholder="Enter Label" class="label_to_logo" name="podcast_settings['+get_int_key+'][label_to_logo]">'+
						'</span>'+
						'<span class="label_anchor_container">'+
							'<input type="text" placeholder="Enter Link to the Label" class="label_anchor" name="podcast_settings['+get_int_key+'][label_anchor]">'+
						'</span>'+
					'</div>'+
					'<div class="close_the_row"><i class="fa fa-times"></i></div>'+
				'</div>';
		$(".podcast_container").append(to_append);
   });
   $(".podcast_container").on("click",".close_container .close_the_row",function(){
		$(this).parents('.podcast_settings_container').remove();
	});
});