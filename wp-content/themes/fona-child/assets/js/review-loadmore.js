 var page = 2;
jQuery(function($) {
    $("#loading_image").hide();
    var ajax_status="completed";
    $(window).scroll(function() {
        if(ajax_status=="completed")
        {
            if($(window).scrollTop() >= $(document).height() - $(window).height()-450)
            {
                ajax_status="pending";
                $("#loading_image").show();
                var data = {
                    'action': 'load_posts_by_ajax',
                    'page': page,
                    'security': review_post_loadmore.wp_create_load_more
                };
                $.post(review_post_loadmore.loadmore_url, data, function(response) {
                    $('.review_content').append(response);
                    page++;
                    $("#loading_image").hide();
                    ajax_status="completed";
                });
            }
        }
    });
});
