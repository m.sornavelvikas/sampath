jQuery(document).ready(function( $ ) {
	
	$(document).on("click", "#home-slider-watch-video", function(){
	  jQuery(".home_slider_video_button .wistia_click_to_play").trigger('click')
	
	});
	
	$("._form_7 input[type=text]").attr("placeholder", "Enter your email").blur();
	jQuery( "#podcast_filter" ).on( "change", function(e) {
		
		var podcast_filter = $("#container_podcast_archive_filter #podcast_filter").val();
		console.log(podcast_filter);
		jQuery.ajax({
					type : 'post',
					url : wordpress_general.ajax_url,
					data : {
						action : 'podcast_filter_function',
						categoryfilter : podcast_filter
					},
					beforeSend: function() {
						jQuery('.podcast_cover_container').addClass('loader-archive');
					},
					success : function( response ) {
						jQuery('#container_podcast_archive_filter #response').html(response);
						jQuery('.podcast_cover_container').removeClass('loader-archive');
					}
				});   
		}).change();
		
		$('body').on('click', '#load_more_posts', function() {
		var podcast_filter = $("#container_podcast_archive_filter #podcast_filter").val();
		var paged = $(this).data("paged") ;
		var total_pages = $(this).data("total_pages");
		jQuery.ajax({
					type : 'post',
					url : wordpress_general.ajax_url,
					data : {
						action : 'load_more_posts',
						categoryfilter : podcast_filter,
						paged : paged
					},
					beforeSend: function() {
						jQuery('.podcast_cover_container').addClass('loader-archive');
					},
					success : function( response ) {
						paged++;
						jQuery('.category_filter_container_module').append(response);
						jQuery('.podcast_cover_container').removeClass('loader-archive');
						if(paged == total_pages ){
							$('#load_more_posts').remove();
						}
						$('#load_more_posts').data('paged',paged);
					}
				});   
		});
});
