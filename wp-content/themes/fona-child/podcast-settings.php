<?php
class PodcastSettings
{
    /**
     * Holds the values to be used in the fields callbacks
     */
    private $options;

    /**
     * Start up
     */
    public function __construct()
    {
        add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
        add_action( 'admin_init', array( $this, 'page_init' ) );
    }

    /**
     * Add options page
     */
    public function add_plugin_page()
	
    {	

		add_submenu_page( 'edit.php?post_type=podcast', 'Podcast Settings Page', 'Podcast Settings', 'administrator', 'postcade-submenu-page', array( $this,'postcade_submenu_page_callback') ); 
    }

    /**
     * Options page callback
     */
    public function postcade_submenu_page_callback()
    {
        // Set class property
        $this->options = get_option( 'podcast_settings' );
        ?>
        <div class="wrap">
            <form method="post" action="options.php">
            <?php
                // This prints out all hidden setting fields
                settings_fields( 'podcast-settings-group' );
                 do_settings_sections( 'postcade-submenu-page' );
                submit_button();
            ?>
            </form>
        </div>
        <?php
    }

    /**
     * Register and add settings
     */
    public function page_init()
    {        
        register_setting(
            'podcast-settings-group', // Option group
            'podcast_settings', // Option name
            array( $this, 'sanitize' ) // Sanitize
        );

        add_settings_section(
            'setting_section_id', // ID
            'Podcast Settings Page', // Title
            array( $this, 'print_section_info' ), // Callback
            'postcade-submenu-page' // Page
        );  


		add_settings_field(
            'podcast_upload_image_button', // ID
            'Upload Icon and Enter respective label to it', // Title 
            array( $this, 'image_upload_button' ), // Callback
            'postcade-submenu-page', // Page
            'setting_section_id' // Section           
        );	
	

      
    }

    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize( $input )
    {
		
        return $input;
    }

    /** 
     * Print the Section text
     */
    public function print_section_info()
    {
        print 'The image and text will be as shortcode displayed in podcast archive and single page';
    }

    /** 
     * Get the settings option array and print one of its values
     */

	public function image_upload_button()
    {	
		
		$options = get_option( 'podcast_settings', array() );
		if(empty($options)){
			$options =array(1);
		}
		$foreach_key = 0;
		echo('<div class="podcast_container">');
		foreach($options as $data){
			
			echo('<div class="podcast_settings_container close_container" data-key ="'.$foreach_key.'"><div class="podcast_upload_settings_container">');
			printf(
				'<img style="width: 100px;" class="podcast-image-preview" src="%s">
				<input type="button" value="Upload Image/Select Image" class="podcast_image_upload"  name="podcast_settings['.$foreach_key.'][podcast_upload_image_button]" />
				<input type="hidden" class="podcast_upload_url" type="text" size="36" id="product_upload_image" name="podcast_settings['.$foreach_key.'][product_upload_image]" value="%s" />',
				isset( $this->options[$foreach_key]['product_upload_image']) ? esc_attr( $this->options[$foreach_key]['product_upload_image']) : '',
				isset( $this->options[$foreach_key]['product_upload_image'] ) ? esc_attr( $this->options[$foreach_key]['product_upload_image']) : ''
			);
			echo('</div><div class="podcast_label_settings_container">');

			$options = get_option( 'podcast_settings', array() );
			$content = isset( $options[$foreach_key]['label_to_logo'] ) ?   $options[$foreach_key]['label_to_logo'] : false;
			printf(
				'<span class="label_to_logo_container"><input type="text" placeholder="Enter Label" value="%s" class="label_to_logo" name="podcast_settings['.$foreach_key.'][label_to_logo]" /></span>
				<span class="label_anchor_container"><input type="text" placeholder="Enter Link to the Label" value="%s" class="label_anchor" name="podcast_settings['.$foreach_key.'][label_anchor]" /></span>',
				isset( $this->options[$foreach_key]['label_to_logo'] ) ? esc_attr( $this->options[$foreach_key]['label_to_logo']) : '',
				isset( $this->options[$foreach_key]['label_anchor'] ) ? esc_attr( $this->options[$foreach_key]['label_anchor']) : ''
			);
			echo('</div><div class="close_the_row"><i class="fa fa-times"></i></div></div>');
			
			$foreach_key++;
		}
		echo('</div>');
		echo('<div class="podcast_reapeter_button"><button class="podcast_reapeter" type="button">Add</button></div>');
	
	}
	
	
}

if( is_admin() )
    $my_settings_page = new PodcastSettings();